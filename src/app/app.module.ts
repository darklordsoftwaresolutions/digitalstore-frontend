//Others
import { TokenInterceptor } from './services/authentication/token-interceptor';
//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms'
import { NgxPaginationModule } from 'ngx-pagination';
import { TemplateModule } from './views/template/template.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RichTextEditorModule } from '@syncfusion/ej2-angular-richtexteditor';
import {NgxPrintModule} from 'ngx-print';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ChartsModule } from 'ng2-charts';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import {ProgressBarModule} from "angular-progress-bar";
import {HttpModule} from '@angular/http';
import { UploaderModule  } from '@syncfusion/ej2-angular-inputs';


//Components
import { AppComponent } from './app.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { LoginComponent } from './views/login/login.component';

//Services
import { MessageService } from './services/message.service';
import { AuthenticationService } from './services/authentication/authentication.service';
import { LoaderService } from './services/loader/loader.service';
import { LoaderComponent } from './views/loader/loader.component';
import { SettingsComponent } from './views/settings/settings.component';
import { AnalyticsComponent } from './views/analytics/analytics.component';
import { ProfileComponent } from './views/profile/profile.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { UsersComponent } from './views/users/users.component';
import { TournamentsComponent } from './views/tournaments/tournaments.component';
import { Dota2Component } from './views/dota2/dota2.component';
import { HeroesComponent } from './views/heroes/heroes.component';
import { LogsComponent } from './views/logs/logs.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { MatchComponent } from './views/match/match.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    LoaderComponent,    
    SettingsComponent,
    AnalyticsComponent,
    ProfileComponent,
    UsersComponent,
    TournamentsComponent,
    Dota2Component,
    HeroesComponent,
    LogsComponent,
    MatchComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ShowHidePasswordModule,
    TemplateModule,
    HttpClientModule,
    PdfViewerModule,
    FormsModule,    
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    NgxAudioPlayerModule ,
    NgSelectModule,
    ColorPickerModule,
    RichTextEditorModule,
    NgxPrintModule,
    ProgressBarModule,
    ChartsModule,
    UploaderModule,
    HttpModule,  
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 4,
      innerStrokeWidth: 2,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      titleColor:"#78C000",
      startFromZero:false,
      animationDuration: 300,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers:
  [
    AuthenticationService,
    LoaderService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
