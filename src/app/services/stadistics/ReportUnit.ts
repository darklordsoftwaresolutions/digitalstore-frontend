export class ReportUnit
{
    public constructor(init?:Partial<ReportUnit>) {
        Object.assign(this, init);
    }   
    label:string;
    data:number[];
  }