export class ReportAV
{
    public constructor(init?:Partial<ReportAV>) {
        Object.assign(this, init);
    }   
    label:string;
    value:number;
}