import { ReportUnit } from './ReportUnit';

export class ReportLineChartByDate
{
    public constructor(init?:Partial<ReportLineChartByDate>) {
        Object.assign(this, init);
    }   
    xLabels:string[];
    values:ReportUnit[];
  }