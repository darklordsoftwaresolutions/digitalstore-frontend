export class UserSumary
{
    public constructor(init?:Partial<UserSumary>) {
        Object.assign(this, init);
    }   
    id_user:number;
    avatar:string
    username:string;
    matchs_played:number;
    matchs_wins:number;
    matchs_louses:number;
    percent:number;
    score:number;
    bestHeroe:number;
    posScore:number;
}