import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../api-global';
import { ReportLineChartByDate } from './ReportLineChartByDate';
import { ReportAV } from './ReportAV';
import { ReportUnit } from './ReportUnit';
import { UserSumary } from './UserSumary';
const api = new API();

@Injectable({
  providedIn: 'root'
})
export class StadisticsService {

  constructor(private http: HttpClient) { }

  totalWins(){
    return this.http.get<ReportAV[]>(api.EnPoints.Stadistics+"/total_wins");
  }

  winsAndLosses(){
    return this.http.get<ReportUnit[]>(api.EnPoints.Stadistics+"/wins_and_loses");
  }

  totalLose(){
    return this.http.get<ReportAV[]>(api.EnPoints.Stadistics+"/total_lose");
  }

  winsPerDate(){
    return this.http.get<ReportLineChartByDate>(api.EnPoints.Stadistics+"/historic_evolution");
  }

  getUserTable(){
    return this.http.get<UserSumary[]>(api.EnPoints.Stadistics+"/sumary_table");
  }
}
