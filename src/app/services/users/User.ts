export class User
{
    public constructor(init?:Partial<User>) {
        Object.assign(this, init);      
    }       
    id:number;
    firstName:string;  
    lastName:string;  
    username:string;  
    password:string;  
    profile:string;
    token:string;   
    credit:number = 0.00;
    createdAt:string;
    createdAtFormatted:string;
    avatar:string;  
    status:number;  
  } 