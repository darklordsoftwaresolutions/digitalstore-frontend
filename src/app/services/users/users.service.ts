import { Injectable } from '@angular/core';
import { API } from '../api-global';
import { HttpClient } from '@angular/common/http';
import { User } from './User';

const api = new API();

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(page:number,maxPerPage:number,filterById:string,filterByUserName:string,filterByFirstName:string,filterByLastName:string)
  {         
    var url = api.EnPoints.Users+"?page="+page+"&maxPerPage="+maxPerPage;    
    if(filterById!="")
       url+="&Id="+encodeURIComponent(filterById);
    if(filterByUserName!="")
       url+="&Username="+encodeURIComponent(filterByUserName);
    if(filterByFirstName!="")
       url+="&FirstName="+encodeURIComponent(filterByFirstName);
    if(filterByLastName!="")
       url+="&LastName="+encodeURIComponent(filterByLastName);
    return this.http.get<User[]>(url,{observe:'response'})
  }  

  getUserById(id:string)
  {    
    var url = api.EnPoints.Users+"/"+encodeURIComponent(id);
    return this.http.get<User>(url);  
  }

  UpdateProfile(user:User)
  {      
    return this.http.put(api.EnPoints.Profile,user);
  }
  
  AddUser(user:User)
  {     
    return this.http.post(api.EnPoints.Users,user);    
  }

  Update(user:User)
  {     
    return this.http.put(api.EnPoints.Users,user);    
  }

  SingIn(user:User)
  {     
    return this.http.post(api.EnPoints.SingIn,user);    
  }

  changeUserStatus(id:string)
  {
    var url = api.EnPoints.Users+"/"+encodeURIComponent(id);
    return this.http.delete(url);
  }

  activeUserById(id:string)
  {
    var url = api.EnPoints.Users+"/"+encodeURIComponent(id);
    return this.http.delete(url);
  }
}
