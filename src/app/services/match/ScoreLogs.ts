export class ScoreLogs
{
    public constructor(init?:Partial<ScoreLogs>) {
        Object.assign(this, init);
    }   
    id:number;
    idMatch:number;
    idUser:number;
    user:number;
    value:number;
    description:number;
  }