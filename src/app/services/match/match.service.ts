import { Injectable } from '@angular/core';
import { API } from '../api-global';
import { HttpClient, HttpResponse, HttpRequest, HttpEvent } from '@angular/common/http';
import {Http, ResponseContentType, RequestOptions} from '@angular/http';
import { Match } from './Match';
import { Join } from './Join';
import { MatchView } from './MatchView';
import { PlayerResult } from './PlayerResult';
import { ScoreLogs } from './ScoreLogs';
import { Observable } from 'rxjs';

const api = new API();

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor(private http: HttpClient,private httpAlt:Http) { }

  get(page:number,maxPerPage:number,filterStatus:string,filterFrom:Date,filterTo:Date)
  {   
    var url = api.EnPoints.Match+"?page="+page+"&maxPerPage="+maxPerPage;   
    if(filterStatus!="-1")
      url+="&Status="+encodeURIComponent(filterStatus.toString());    
    if(filterFrom!=null)
      url+="&From="+encodeURIComponent(filterFrom.toString());
    if(filterTo!=null)
      url+="&To="+encodeURIComponent(filterTo.toString());

    return this.http.get<Match[]>(url,{observe:'response'})
  }  

  getById(id:string)
  {    
    var url = api.EnPoints.Match+"/"+encodeURIComponent(id);
    return this.http.get<Match>(url);  
  }

  initMatch(id:string)
  {     
    return this.http.put(api.EnPoints.Match+"/"+id,{});    
  }

  cancel(id:string)
  {
    var url = api.EnPoints.Match+"/"+encodeURIComponent(id);
    return this.http.delete(url);
  }

  join(join:Join){
    return this.http.post(api.EnPoints.Match+"/join",join);
  }

  leave(leave:Join){
    return this.http.post(api.EnPoints.Match+"/leave",leave);
  }

  matchStatus(id:string){
    return this.http.get<MatchView>(api.EnPoints.Match+"/matchstatus/"+id);
  }

  saveResult(pr:PlayerResult,idMatchResult:number,idPlayer:number){
    var url = api.EnPoints.Match+"/playerresult?idMatch="+idMatchResult+"&idPlayer="+idPlayer;
    return this.http.post(url,pr);
  }

  loadResult(idMatchResult:number,idPlayer:number){
    var url = api.EnPoints.Match+"/playerresult_get?idMatch="+idMatchResult+"&idPlayer="+idPlayer;
    return this.http.get<PlayerResult>(url);
  }

  add(item:Match)
  {     
    return this.http.post(api.EnPoints.Match,item);    
  }

  endMatch(form:FormData){
    return this.http.post(api.EnPoints.Match+"/endmatch",form,{reportProgress: true, observe: 'events'});
  }

  getCombatLogs(idMatch:number,idPlayer:number){
    var url = api.EnPoints.Match+"/match_logs?idMatch="+idMatch+"&idPlayer="+idPlayer;
    return this.http.get<ScoreLogs[]>(url);
  }

  downloadReplay(idMatch:number): Observable<HttpEvent<Blob>> {
    var url = api.EnPoints.Match+"/download?idMatch="+idMatch;
    const options = { responseType: 'blob' as 'json',reportProgress: true }
    return this.http.get<HttpEvent<Blob>>(url, options);
  } 

  

  downloadFile(idMatch:number): Observable<HttpEvent<Blob>> {
    var url = api.EnPoints.Match+"/download?idMatch="+idMatch;
    return this.http.request(new HttpRequest(
      'GET',
      url,
      {
        reportProgress: true,
        responseType: 'blob'
      }));
  }
}
