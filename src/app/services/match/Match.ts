export class Match
{
    public constructor(init?:Partial<Match>) {
        Object.assign(this, init);
    }   
    id:number;
    createdAtFormatted:string;
    idUserPublisher:number;
    createdBy:string;
    createdAt:string;
    status:number;
    photo:string;
    winner:number;
    type:number;
    replayFile:string;

    radiant1:number;
    radiant2:number;
    radiant3:number;
    radiant4:number;
    radiant5:number;

    uradiant1:string;
    uradiant2:string;
    uradiant3:string;
    uradiant4:string;
    uradiant5:string;
    
    dire1:number;   
    dire2:number;  
    dire3:number;  
    dire4:number;  
    dire5:number;  

    udire1:string;   
    udire2:string;  
    udire3:string;  
    udire4:string;  
    udire5:string;

    teamDire:string;
    teamRadiant:string;
    
  }