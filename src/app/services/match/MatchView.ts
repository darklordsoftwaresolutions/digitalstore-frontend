import { Player } from './Player';

export class MatchView
{
    public constructor(init?:Partial<MatchView>) {
        Object.assign(this, init);
    }   
    IdMatch:number;
    player1:Player;
    player2:Player;   
    player3:Player;
    player4:Player;
    player5:Player;
    player6:Player;
    player7:Player;
    player8:Player;
    player9:Player;
    player10:Player;


    scoreRadiant1:number; 
    scoreRadiant2:number; 
    scoreRadiant3:number; 
    scoreRadiant4:number; 
    scoreRadiant5:number; 

    scoreDire1:number; 
    scoreDire2:number; 
    scoreDire3:number; 
    scoreDire4:number; 
    scoreDire5:number; 

  }