export class Player
{
    public constructor(init?:Partial<Player>) {
        Object.assign(this, init);
    }   
    idUser:number;
    heroPhoto:string;
    name:string;
    hero:string;
    heroId:number;
    rol:number; 
    kills:number;
    assistences:number;
    dies:number;
    level:number;   
    score:number;
    wasBestPlayer:boolean;
    wasWorstPlayer:boolean;
  }