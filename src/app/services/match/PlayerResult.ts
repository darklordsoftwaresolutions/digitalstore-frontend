export class PlayerResult
{
    public constructor(init?:Partial<PlayerResult>) {
        Object.assign(this, init);
    }   
    id:number;
    dies:number;
    kills:number;
    assistans:number;
    damageTaked:number;
    damageMaked:number;
    damageBlocked:number;
    hpHealed:number;
    manaRestore:number;
    netValue:number;
    creeps:number;
    creepsDeny:number;
    wards:number;
    wasBestPlayer:boolean;
    wasWorstPlayer:boolean;
    level:number;
    stunSeconds:number;
    silenceSeconds:number;
    gracePoints:number;
    win:number;
  }