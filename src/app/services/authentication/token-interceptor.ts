import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { API } from '../api-global';

const api = new API();
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> { 
        if (request.url!=api.EnPoints.Login && this.authenticationService.isLogin()) {            
            request = request.clone({
                setHeaders: {
                    'authorization': "Bearer "+localStorage.getItem('token')
                }
            });
        }
        return next.handle(request);
    }
}