import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http'
import { tap } from 'rxjs/operators';
import {API, MessageType} from '../api-global'
import { Router } from '@angular/router';
import { LoaderService } from '../loader/loader.service';
import * as jwt_decode from "jwt-decode";
import { TranslateService } from '@ngx-translate/core';
import { String, StringBuilder } from 'typescript-string-operations';
import { Login } from './Login';
import { User } from '../users/User';
import * as CryptoJS from 'crypto-js'; 




const api = new API();

const pass = "99bbc-3725d-4631-cdf9-078a-bccd";
const session = "sessionStorage";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  constructor(public translate:TranslateService,private router: Router,private http: HttpClient,private progress: LoaderService) {}  

  logOut()
  {
    let lang = localStorage.getItem("LANG");
    localStorage.clear();
    localStorage.setItem("LANG",lang);
    this.router.navigate(['/login']);
  }

  isLogin()
  {    
    return (localStorage.getItem("token")!=undefined&&localStorage.getItem(session)!=undefined);
  }

  saveSession(json:User){   
    let js = JSON.stringify(json);  
    localStorage.setItem(session,CryptoJS.AES.encrypt(js,pass).toString());    
  }

  public loadSession(){
    try{
      let encrip = localStorage.getItem(session);   
      let descript = CryptoJS.AES.decrypt(encrip,pass).toString(CryptoJS.enc.Utf8);       
      let user:User = JSON.parse(descript);  
      return user;
    }
    catch(e){
      return null;
    }    
  }

  verifyAccess()
  {
    if(!this.isLogin())
    {
      this.logOut();
      return false;
    }
       return true;
  }

  getUserLogin()
  {
    if(this.isLogin())
    {
      let token = localStorage.getItem("token");
      let tokenInfo = jwt_decode(token);      
      return new User({firstName:tokenInfo[api.ClaimTypes.Name],username:tokenInfo[api.ClaimTypes.User],profile:tokenInfo[api.ClaimTypes.Rol]});
    }
    return new User({username:"error-loading"});
  }

  Authenticade(user:string, password:string)
  {  
    this.progress.show();
  
    const headers = new HttpHeaders({
      'Content-Type':  'application/json'
    });
    let data = new Login({Username:user,Password:password});    
    this.http.post<User>(api.EnPoints.Login,data,{headers,responseType: 'json'}).subscribe(user=>
    {
      api.Message(MessageType.Success,String.Format(this.translate.instant('LOGIN.LOGIN_SUCCESSFUL'),user.firstName+" "+user.lastName),this.translate.instant('LOGIN.LOGIN_SUCCESSFUL_MSG')); 
      localStorage.setItem('token',user.token);
      this.saveSession(user);
      window.location.href='/dashboard';              
      this.progress.hidde();
    },
    error=>
    {
      this.PosibleErrors(error);
      this.progress.hidde();
    }
    );
  }

  PosibleErrors(error:HttpErrorResponse)
  {
    switch(error.status)
       {
        case 0:
         api.Message(MessageType.Error,"Existen Problemas Técnicos","En estos momentos el servidor de Datos no está disponible, contacte a su administrador para las dudas, intente un poco más tarde.");         
          break;
         case 401:
         api.Message(MessageType.Error,"Error de Credenciales","Contraseña incorrecta, verifique la contraseña.");         
         break;
         case 403:
         api.Message(MessageType.Error,"La api es inaccesible","Verifique su conexión o contacte al administrador.");         
         break; 
         case 404:
          api.Message(MessageType.Error,"La Cuenta no Existe","La cuenta no existe o no está activa, intente rectificar la cuenta o contacte al administrador.");         
          break; 
          default:
          api.Message(MessageType.Error,String.Format("Error Desconocido (Código:{0})",error.status),error.message);        
          break; 
       }
  }
}
