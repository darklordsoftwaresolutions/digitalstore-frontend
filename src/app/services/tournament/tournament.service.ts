import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../api-global';
import { Tournament } from './Tournament';

const api = new API();

@Injectable({
  providedIn: 'root'
})
export class TournamentService {

  constructor(private http: HttpClient) { }

  getAll(page:number,maxPerPage:number,filterById:string,filterName:string,filterByStatus:string,filterByType:string,filterStartDate:string,filterEndDate:string)
  {         
    var url = api.EnPoints.Tournament+"?page="+page+"&maxPerPage="+maxPerPage;    
    if(filterById!="")
       url+="&Id="+encodeURIComponent(filterById);
    if(filterName!="")
       url+="&Name="+encodeURIComponent(filterName);
    if(filterByStatus!="")
       url+="&Status="+encodeURIComponent(filterByStatus);
    if(filterByType!="")
       url+="&Type="+encodeURIComponent(filterByType);
    if(filterStartDate!="")
       url+="&startDate="+encodeURIComponent(filterStartDate);
    if(filterEndDate!="")
       url+="&endDate="+encodeURIComponent(filterEndDate);
    return this.http.get<Tournament[]>(url,{observe:'response'})
  }

  getById(id:string)
  {    
    var url = api.EnPoints.Tournament+"/"+encodeURIComponent(id);
    return this.http.get<Tournament>(url);  
  }

  add(item:Tournament)
  {     
    return this.http.post(api.EnPoints.Tournament,item);    
  }

  update(item:Tournament)
  {     
    return this.http.put(api.EnPoints.Tournament,item);    
  }

  delete(id:string){
    var url = api.EnPoints.Tournament+"/"+encodeURIComponent(id);
    return this.http.delete(url);
  }
}
