export class Tournament
{
    public constructor(init?:Partial<Tournament>) {
        Object.assign(this, init);      
    }       
    id:number;
    name:string;  
    type:number;
    createdAt:string;
    createdAtFormatted:string;
    photo:string;  
    status:number;  
  } 