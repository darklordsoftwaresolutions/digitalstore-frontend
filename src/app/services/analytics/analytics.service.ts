import { Injectable } from '@angular/core';
import { API } from '../api-global';
import { LastVisits } from './LastVIsits';
import { HttpClient } from '@angular/common/http';
import { Statistics } from './Statistics';
import { HoursVisit } from './HoursVisits';

const api = new API();
@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  constructor(private http: HttpClient) { }

  getLastDaysVisits(days:number)
  {
    let url = api.EnPoints.AnalyticsLastVisits+"/"+days;
    return this.http.get<LastVisits[]>(url,{observe:'response'});
  }

  getStaticsFromLastDays(days:number)
  {
    let url = api.EnPoints.AnalyticsStatistics+"/"+days;
    return this.http.get<Statistics>(url,{observe:'response'});
  }

  getStaticsHoursLastDays(days:number)
  {
    let url = api.EnPoints.AnalyticsVisitsHours+"/"+days;
    return this.http.get<HoursVisit[]>(url,{observe:'response'});
  }


}
