export class HoursVisit
{
    public constructor(init?:Partial<HoursVisit>) {
        Object.assign(this, init);
    }     
    totalHours: number;
    date: string;   
  }