export class Statistics
{
    public constructor(init?:Partial<Statistics>) {
        Object.assign(this, init);
    }     
    averageVisit:number;
    halfVisit:number;
    maxVisit:number;
    minVisit:number;
    averageVisitWatchList:number;
    halfVisitWatchList:number;
    maxVisitWatchList:number;
    minVisitWatchList:number;
    averageVisitOK:number;
    halfVisitOK:number;
    maxVisitOK:number;
    minVisitOK:number;
    totalVisit:number;
    totalVisitOK:number;
    totalVisitWatchList:number;  
  }