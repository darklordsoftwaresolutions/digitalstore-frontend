export class LastVisits
{
    public constructor(init?:Partial<LastVisits>) {
        Object.assign(this, init);
    }     
    date: string;
    countVisitWatchList: number;
    countVisitOK: number;
    countVisit: number;  
  }