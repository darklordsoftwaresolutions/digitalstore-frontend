import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }
  private api = 'https://localhost:44350/files/';

  private accessToken = 'yourapitoken';

  createVideo(file: File): Observable<any> {
    const body = {
      name: file.name,
      upload: {
        approach: 'tus',
        size: file.size
      }
    };
    console.log(file);
    const header: HttpHeaders = new HttpHeaders()      
      .set('Content-Type', 'application/octet-stream')   
    return this.http.post(this.api, body, {
      headers: header,
      observe: 'response'
    });
  }

}
