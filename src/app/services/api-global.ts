import PNotify from 'pnotify/dist/es/PNotify';
import PNotifyButtons from 'pnotify/dist/es/PNotifyButtons';
import { Match } from './match/Match';
import { MatchService } from './match/match.service';
import { HttpEventType } from '@angular/common/http';
import { DownloadItem } from '../views/template/DownloadItem';

declare var require: any

export enum MessageType
{
    Error,
    Info,
    Success,
    Warning
}

export var downloadList:DownloadItem [] = [];

export class API
{
    public ApiUrl = "https://localhost:44300/";
  //  public ApiUrl ="http://192.168.3.37:88/";

    
    
    public EnPoints= 
    {
        Login : this.ApiUrl + "api/login",
        Users : this.ApiUrl + "api/users",
        SingIn: this.ApiUrl + "api/users/singin",
        Profile: this.ApiUrl + "api/users/profile",
        Tournament: this.ApiUrl + "api/Tournaments",
        Logs: this.ApiUrl + "api/logs",
        Heroes: this.ApiUrl + "api/heroes",
        Match: this.ApiUrl + "api/matches",   
        Stadistics: this.ApiUrl + "api/staditics",  

        Persons : this.ApiUrl + "api/person",
        Visitors: this.ApiUrl + "api/visitor",
        VisitorsImport: this.ApiUrl + "api/visitor/import",
        VisitorTimeLogs: this.ApiUrl + "api/visitor/visitorlog",
        VisitorWhoInWhoOut: this.ApiUrl + "api/visitor/visitorwhoisoutininside",
        VisitorDetailReport: this.ApiUrl + "api/visitor/visitordetailedreport",       
        VisitorSummaryReport: this.ApiUrl + "api/visitor/visitorsummaryreport",     
        VisitorWatchReport: this.ApiUrl + "api/visitor/visitorwatchlistreport",
        VisitorWatchReportPrint: this.ApiUrl + "api/visitor/visitorWatchListReportPrint",
        Timelogs: this.ApiUrl + "api/rawtimeLog",
        TimeZone: this.ApiUrl + "api/location/timezones",
        Locations: this.ApiUrl + "api/location",
        Settings: this.ApiUrl + "api/settings",
        AnalyticsLastVisits:this.ApiUrl + "api/statistic/visitbylastdaysanalystic",
        AnalyticsStatistics:this.ApiUrl + "api/statistic/visitbylastdays",
        AnalyticsVisitsHours:this.ApiUrl + "api/statistic/hoursperdayanalystic",
        VisitorType:this.ApiUrl + "api/visitortype"
    }    
    public ClaimTypes = 
    {
        Name: "name",
        User:"user",
        Rol: "role"
    }
    constructor()
    {
        PNotifyButtons;   
    }

    getInitToday()
    {
        let today=new Date();
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
        return today;
    }

    getEndToday()
    {
        let today=new Date();
        today.setHours(23);
        today.setMinutes(59);
        today.setSeconds(59);
        return today;
    }


    formattedDate(date:Date)
    {
       return date.toLocaleDateString('en-US',{ day:'2-digit',month: '2-digit',year:'numeric'}) +" "+ new Date(date).toLocaleTimeString('en-US', { hour: '2-digit', hour12: true, minute: '2-digit' });
    }
   
    Message(type:MessageType,titleMsg:string,msg:string)
    {
        let typeError= "notice";

        switch(type)
        {
            case MessageType.Error:
            typeError = "error";
            break;
            case MessageType.Info:
            typeError = "info";
            break;
            case MessageType.Success:
            typeError = "success";
            break;
            case MessageType.Warning:
            typeError = "notice";
            break;
        }

        PNotify.alert({
            title: titleMsg,
            text: msg,
            type: typeError
          });   
    }

     groupBy(xs, key) {
        return xs.reduce(function(rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
      };

    timeDiffHHMM(d1:Date,d2:Date)
    {
        var timespan = require('timespan');     
        var ts = new timespan.TimeSpan(d1.getTime()-d2.getTime());   
        let hours = ts.hours<10?"0"+ts.hours:ts.hours;   
        let minutes = ts.minutes<10?"0"+ts.minutes:ts.minutes;  
        return `${hours}:${minutes}`;
    }
}
