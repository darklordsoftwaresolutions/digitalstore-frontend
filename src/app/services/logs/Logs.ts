export class Logs
{
    public constructor(init?:Partial<Logs>) {
        Object.assign(this, init);
    }   
    id:number;
    title:string;
    msg:string;
    type:number;
    createdAt:string;
    user:String;
    createdAtFormatted:string;
  }