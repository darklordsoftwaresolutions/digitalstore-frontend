import { Injectable, Type } from '@angular/core';
import { API } from '../api-global';
import { HttpClient } from '@angular/common/http';
import { Logs } from './Logs';

const api = new API();

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(private http: HttpClient) { }

  get(page:number,maxPerPage:number,filterById:string,filterByUser:string,filterByTitle:string,filterByMsg:string,filterByFrom:string,filterByTo:string,filterByType:number)
  {         
    var url = api.EnPoints.Logs+"?page="+page+"&maxPerPage="+maxPerPage;    
    if(filterById!="")
       url+="&Id="+encodeURIComponent(filterById);
    if(filterByUser!="")
       url+="&User="+encodeURIComponent(filterByUser);
    if(filterByTitle!="")
       url+="&Title="+encodeURIComponent(filterByTitle);
    if(filterByMsg!="")
       url+="&Msg="+encodeURIComponent(filterByMsg);
    if(filterByFrom!="")
       url+="&From="+encodeURIComponent(filterByFrom);
    if(filterByTo!="")
       url+="&To="+encodeURIComponent(filterByTo);
    if(filterByType!=-1)
       url+="&Type="+encodeURIComponent(filterByType.toString());

    return this.http.get<Logs[]>(url,{observe:'response'})
  }  
}
