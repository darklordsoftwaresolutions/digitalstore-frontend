import { Injectable } from '@angular/core';
import { LoaderService } from '../loader/loader.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API } from '../api-global';
import { Settings } from './Settings';
const api = new API();
@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private http: HttpClient,private progress: LoaderService) { }

  getAllSettings()
  {     
    let url = api.EnPoints.Settings;   
    return this.http.get<Settings[]>(url,{observe:'response'});
  } 

  getSettingsByField(field:string)
  {     
    let url = api.EnPoints.Settings+"/"+encodeURIComponent(field);   
    return this.http.get<Settings>(url,{observe:'response'});
  } 

  updateOrCreateSettings(usettings:Settings)
  {
    const headers = new HttpHeaders({     
    });
    return this.http.put(api.EnPoints.Settings,usettings,{headers,responseType: 'text'});
  }
}
