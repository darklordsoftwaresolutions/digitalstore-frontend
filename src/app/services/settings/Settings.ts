export class Settings
{
    public constructor(init?:Partial<Settings>) {
        Object.assign(this, init);
    }   
    id:number;
    field:string;
    value:string;
  }