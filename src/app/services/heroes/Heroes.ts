export class Heroe
{
    public constructor(init?:Partial<Heroe>) {
        Object.assign(this, init);
    }   
    id:number;
    name:string;
    photo:string;
    type:number;    
  }