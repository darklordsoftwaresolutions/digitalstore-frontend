import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../api-global';
import { Heroe as Heroe } from './Heroes';

const api = new API();

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  constructor(private http: HttpClient) { }

  get(page:number,maxPerPage:number,filterByName:string,filterByType:number)
  {   
    var url = api.EnPoints.Heroes+"?page="+page+"&maxPerPage="+maxPerPage;   
    if(filterByName!="")
      url+="&Name="+encodeURIComponent(filterByName); 
    if(filterByType!=-1)
       url+="&Type="+encodeURIComponent(filterByType.toString());

    return this.http.get<Heroe[]>(url,{observe:'response'})
  }  

  getLite()
  {   
    var url = api.EnPoints.Heroes+"/lite";   
    return this.http.get<Heroe[]>(url,{observe:'response'})
  }  

  getById(id:string)
  {    
    var url = api.EnPoints.Heroes+"/"+encodeURIComponent(id);
    return this.http.get<Heroe>(url);  
  }

  update(heroe:Heroe)
  {     
    return this.http.put(api.EnPoints.Heroes,heroe);    
  }

  delete(id:string)
  {
    var url = api.EnPoints.Heroes+"/"+encodeURIComponent(id);
    return this.http.delete(url);
  }

  add(hero:Heroe)
  {     
    return this.http.post(api.EnPoints.Heroes,hero);    
  }
}
