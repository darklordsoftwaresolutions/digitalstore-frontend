import { Component, OnInit } from '@angular/core';
import { API, MessageType } from 'src/app/services/api-global';
import { Logs } from 'src/app/services/logs/Logs';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { LogsService } from 'src/app/services/logs/logs.service';
import { HttpErrorResponse } from '@angular/common/http';
import { String, StringBuilder } from 'typescript-string-operations';
import { formatDate } from '@angular/common';

const api = new API();

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  list: Logs[] = [];  
  total: number = 0;
  page: number = 1;
  maxPerPage: number = 25;
  filterId: string = "";
  filterTitle: string = "";
  filterMsg: string = "";
  filterUser: string = "";
  filterFrom: string = "";
  filterTo: string = "";  
  filterType: number = -1;

  constructor(private loader: LoaderService, private logsService: LogsService, private authentication: AuthenticationService) { }

  ngOnInit() {
    this.getTable();
  }

  getTable(){
    this.loader.show();
    this.logsService.get(this.page,this.maxPerPage,this.filterId,this.filterUser,this.filterTitle,this.filterMsg,this.filterFrom,this.filterTo,this.filterType).subscribe(data=>{
      this.list = data.body;
      this.list.forEach(element => {
        element.createdAtFormatted = formatDate(new Date(element.createdAt), "yyyy-MM-dd hh:mm a", "en-US");
      });
      this.total = +data.headers.get("X-Total-Count");     
      this.loader.hidde();
    },error=>this.PosibleErrors(error));
  }

  setFilterFrom(date){
    let tester:Date = new Date(date.value);        
    if(isNaN(tester.getTime()))
    {    
      date.value = "";
      this.filterFrom ="";
    }   
    else
    {      
      this.filterFrom = tester.toISOString();      
    }
    this.getTable();
  }

  setFilterTo(date){
    let tester:Date = new Date(date.value);        
    if(isNaN(tester.getTime()))
    {    
      date.value = "";
      this.filterTo ="";
    }   
    else
    {      
      this.filterTo = tester.toISOString();      
    }
    this.getTable();
  }

  setIdFilter(filter:string){
    this.filterId = filter;
    this.getTable();

  }

  setUserFilter(filter:string){
    this.filterUser = filter;
    this.getTable();
  }

  setTitleFilter(filter:string){
    this.filterTitle = filter;
    this.getTable();
  }

  setMsgFilter(filter:string){
    this.filterMsg = filter;
    this.getTable();
  }

  setTypeFilter(filter:number){
    this.filterType = filter;
    this.getTable();
  }

  PosibleErrors(error: HttpErrorResponse) {
    this.loader.hidde();
    switch (error.status) {
      case 0:
          api.Message(MessageType.Error,"Existen Problemas Técnicos","En estos momentos el servidor de Datos no está disponible, contacte a su administrador para las dudas, intente un poco más tarde.");         
        break;
      case 401:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Ha expirado su sessión", "Necesita acceder nuevamente.");
        break;
      case 403:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Falta de Permisos", "Usted no tiene permisos para realizar esa acción.");
        break;      
      default:
        api.Message(MessageType.Error, String.Format("Error Desconocido (Código:{0})", error.status), error.message);
        break;
    }
  }

}
