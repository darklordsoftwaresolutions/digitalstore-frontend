import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { TranslateService } from '@ngx-translate/core';
import { AnalyticsService } from 'src/app/services/analytics/analytics.service';
import { String, StringBuilder } from 'typescript-string-operations';

import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { HttpErrorResponse } from '@angular/common/http';
import { API, MessageType } from 'src/app/services/api-global';

const last_days: number = 10;
const api = new API();

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  public lineChartData: ChartDataSets[] = [
    { data: [0] }
  ];
  public lineChartLabels: Label[] = [''];
  public timeChartData: ChartDataSets[] = [
    { data: [0] }
  ];
  public timeChartLabels: Label[] = [''];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {

      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [

      ],
    },
  };
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;


  public barChartData: ChartDataSets[] = [
    { data: [0] }
  ];

  public barChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['BLA', 'BLU'];
  public pieChartData: number[] = [53, 14];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(0,255,0,0.3)', 'rgba(255,0,0,0.3)'],
    },
  ];

  @ViewChild(BaseChartDirective, { read: true }) chart: BaseChartDirective;

  constructor(private loader: LoaderService, private authentication: AuthenticationService, public translate: TranslateService, private analytic: AnalyticsService) {

  }

  ngOnInit() {

    this.loadLastVisit();
  }

  loadLastVisit() {
    this.analytic.getLastDaysVisits(last_days).subscribe((data) => {
      let result = data.body;
      this.lineChartLabels = [];
      this.lineChartData = [];
      this.barChartLabels = [];
      this.barChartData = [];
      let total_visits = [];
      let visitors_ok = [];
      let visitors_inwl = [];
      let count = result.length;
      let acumulate = 0;
      result.forEach(element => {
        acumulate += element.countVisit;
        total_visits.push(element.countVisit);
        visitors_ok.push(element.countVisitOK);
        visitors_inwl.push(element.countVisitWatchList);
        this.lineChartLabels.push(element.date);
        this.barChartLabels.push(element.date);
      });
      let average = [];

      for (let i = 0; i < count; i++)
        average.push(acumulate / count);


      this.lineChartData.push({ data: total_visits, label: String.Format(this.translate.instant("ANALYTICS.LAST_VISITS"), last_days) });
      this.lineChartData.push({ data: average, label: this.translate.instant("ANALYTICS.AVERAGE") });
      this.barChartData.push({ data: visitors_ok, label: this.translate.instant("ANALYTICS.LAST_VISITS_OK") });
      this.barChartData.push({ data: visitors_inwl, label: this.translate.instant("ANALYTICS.LAST_VISITS_IWL") });
      this.chart.update();

    }, error => { this.PosibleErrors(error) });

    this.analytic.getStaticsHoursLastDays(last_days).subscribe((data) => {
      let result = data.body;   
      let total_time = [];
      this.timeChartData = [];
      this.timeChartLabels = [];

      result.forEach(element => {
        this.timeChartLabels.push(element.date);
        total_time.push(element.totalHours)
      });

      this.timeChartData.push({ data: total_time, label: this.translate.instant("ANALYTICS.TIME_OF_LAST_VISITS") });
      this.chart.update();

    }, error => { this.PosibleErrors(error) });

    this.analytic.getStaticsFromLastDays(last_days).subscribe((data) => {
      let result = data.body;
      this.pieChartLabels = [this.translate.instant("ANALYTICS.LAST_VISITS_OK"), this.translate.instant("ANALYTICS.LAST_VISITS_IWL")];
      this.pieChartData = [result.totalVisitOK, result.totalVisitWatchList];
      this.chart.update();

    }, error => { this.PosibleErrors(error) });
  }

  PosibleErrors(error: HttpErrorResponse) {
    this.loader.hidde();
    switch (error.status) {
      case 401:
        this.authentication.logOut();
        api.Message(MessageType.Error, this.translate.instant("ERRORS.SESSION_EXPIRED"), this.translate.instant("ERRORS.PLEASE_LOGIN_AGAIN"));
        break;
      case 403:
        api.Message(MessageType.Error, this.translate.instant("ERRORS.API_UNACCESIBLE"), this.translate.instant("ERRORS.NOT_ACCESS_TO_API"));
        break;
      default:
        api.Message(MessageType.Error, String.Format(this.translate.instant("ERRORS.UNKNOW_ERROR_CODE"), error.status), error.message);
        break;
    }
  }

}
