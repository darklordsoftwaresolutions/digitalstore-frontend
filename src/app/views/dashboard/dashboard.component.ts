import { Component, OnInit, ViewChild } from '@angular/core';
import { API, MessageType } from 'src/app/services/api-global';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { String, StringBuilder } from 'typescript-string-operations';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import 'chartjs-plugin-colorschemes';
import 'chartjs-plugin-colorschemes/src/plugins/plugin.colorschemes'; 
const api = new API();

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 

  constructor(public translate: TranslateService,private loader: LoaderService,private authentication:AuthenticationService) { }
  
  ngOnInit() {
    
  }
}
