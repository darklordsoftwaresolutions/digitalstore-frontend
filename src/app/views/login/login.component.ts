import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms'
import { String, StringBuilder } from 'typescript-string-operations';
import {API, MessageType} from '../../services/api-global';
import config from '../../../assets/config/settings.json';
import { AuthenticationService } from '../../services/authentication/authentication.service'
import { TranslateService } from '@ngx-translate/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LoaderService } from '../../services/loader/loader.service';
import { User } from '../../services/users/User';
import { UsersService } from '../../services/users/users.service';

const api = new API();
const max_height_img_allowed = 1024;
const max_width_img_allowed = 1024;
declare var jQuery:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [  AuthenticationService ]
})

export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  newUserForm: FormGroup;
  newUserSubmitted:boolean = false;
  get fnu() {return this.newUserForm.controls;}

  submitted = false;
  facility:string = "Access Control";
  logo:string = "../../../assets/images/visitors.png";
  constructor(private loader: LoaderService,public translate:TranslateService,private  autheticationService: AuthenticationService,private formBuilder: FormBuilder,private usersProfileService:UsersService) 
  {    
    translate.addLangs(['en','es','fr']);
    translate.setDefaultLang('en');
    let browserLang = translate.getBrowserLang();
    if(localStorage.getItem("LANG")!=null) 
      browserLang = localStorage.getItem("LANG");    
    else
      localStorage.setItem("LANG",browserLang);    
    translate.use(browserLang.match("en|fr|es") ? browserLang : 'en');    
  }
  
  ngOnInit() {
    this.loginForm = this.formBuilder.group(
      {        
        user: ['',Validators.required],
        password:[]
      }
    );   
    this.newUserForm = this.formBuilder.group({
      userNameNew:['',Validators.required],
      userFirstNameNew:['',Validators.required],
      lastNameNew:['',Validators.required],
      passwordNew:['',Validators.required],
      passwordRepeatNew:['',Validators.required],
      imageUser:['']
    });
    this.facility = config["FACILITY_NAME"];
    this.logo = config["LOGO"];
   
    document.getElementById("Logo").setAttribute("src",this.logo);
  }

  newUser(){
    this.newUserSubmitted =  true;
    if(this.newUserForm.invalid)
    {      
      return false;
    }
    
    let NewUser:string = this.newUserForm.get("userNameNew").value;   
    let NewFirstName:string = this.newUserForm.get("userFirstNameNew").value; 
    let NewLastName:string = this.newUserForm.get("lastNameNew").value;   
    let passwordNew = this.newUserForm.get("passwordNew").value;
    let passwordRepeatNew = this.newUserForm.get("passwordRepeatNew").value;
    let src = document.getElementById("user_photo").getAttribute("src");

    if(passwordRepeatNew!=passwordNew){
      api.Message(MessageType.Error,"Las Contraseñas no coinciden","Debe repetir la contraseña para continuar");
      return false;
    }

    let nUser = new User({firstName:NewFirstName,lastName:NewLastName,username:NewUser,password:passwordNew,avatar:src});   

    this.loader.show();
    
    this.usersProfileService.SingIn(nUser).subscribe((success)=>
      {        
        api.Message(MessageType.Success,"Cuenta Creada",String.Format("La cuenta de {0} ha sido creada satisfactoriamente.",nUser.firstName+" "+nUser.lastName));                
        jQuery("#new_user").modal("hide");
        this.newUserForm.reset();     
        document.getElementById("user_photo").setAttribute('src',"../../../assets/images/default_user.png");            
        this.loader.hidde(); 
      },(error=>{this.PosibleErrors(error);this.loader.hidde();}));
    this.newUserSubmitted =  false;   
    return false;  
  }

  getBase64(file:any) 
  {  
    let reader = new FileReader();
    let _file = file.files[0];    
    if(_file.type=="image/jpeg" || _file.type=="image/jpg" || _file.type=="image/png" )
    {      
      reader.readAsDataURL(_file);
      reader.onload = function() {
        let img = new Image();
        img.onload = function (event) 
        {
          let loadedImage: any = event.currentTarget;          
          if(loadedImage.height>max_height_img_allowed||loadedImage.width>max_width_img_allowed)
            api.Message(MessageType.Error,"La Imagen muy Grande","La máxima resolución es "+max_height_img_allowed+"x"+max_width_img_allowed+".");
          else
          { 
            document.getElementById("user_photo").setAttribute('src',img.src);            
            document.getElementById("selectedAvatar").innerHTML = _file.name
          }             
        };
        img.src = reader.result.toString();   
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error,"Error Cargando la Imagen","Ha ocurrido un error leyendo la imagen, intente nuevamente.");
      };
    }
    else
       api.Message(MessageType.Error,"Formato no soportado","Solo se permiten los formatos JPEG,JPG or PNG.");
    file.value=""
  }

  Login(User:string,Password:string)
  {        
    this.submitted = true;
    if(this.loginForm.invalid)
    {
      return;
    }
    this.autheticationService.Authenticade(User,Password);   
    return false;
  }

  PosibleErrors(error:HttpErrorResponse)
  {   
    
    switch(error.status)
       {
        case 0:
          api.Message(MessageType.Error,"Existen Problemas Técnicos","En estos momentos el servidor de Datos no está disponible, contacte a su administrador para las dudas, intente un poco más tarde.");         
        break;
         case 401:          
         this.autheticationService.logOut();   
         api.Message(MessageType.Error,"Ha expirado su sessión","Necesita acceder nuevamente.");     
         break;
         case 403:
         api.Message(MessageType.Error,"La api es inaccesible","Verifique su conexión o contacte al administrador.");         
         break;            
         case 409:
         api.Message(MessageType.Error,"Ya existe esa Cuenta","La cuenta que está intentando crear ya existe.");         
         break;       
         default:
         api.Message(MessageType.Error,String.Format("Error Desconocido (Código:{0})",error.status),error.message);        
         break; 
       }       
  }

  get f() {return this.loginForm.controls;}
}