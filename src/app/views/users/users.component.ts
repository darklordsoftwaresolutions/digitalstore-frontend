import { Component, OnInit } from '@angular/core';
import { API, MessageType } from '../../services/api-global';
import { User } from '../../services/users/User';
import { UsersService } from '../../services/users/users.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { LoaderService } from '../../services/loader/loader.service';
import { String, StringBuilder } from 'typescript-string-operations';
import { formatDate } from '@angular/common';

const api = new API();
const max_height_img_allowed = 1024;
const max_width_img_allowed = 1024;
declare var jQuery: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  selected: User = new User();
  total: number = 0;
  page: number = 1;
  maxPerPage: number = 10;
  filterId: string = "";
  filterUser: string = "";
  filterFirstName: string = "";
  filterLastName: string = "";

  newUserForm: FormGroup;
  newUserSubmitted: boolean = false;
  get fnu() { return this.newUserForm.controls; }

  editUserForm: FormGroup;
  editUserSubmitted: boolean = false;
  get feu() { return this.editUserForm.controls; }

  constructor(private loader: LoaderService, private userService: UsersService, private authentication: AuthenticationService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.newUserForm = this.formBuilder.group({
      userNameNew: ['', Validators.required],
      userFirstNameNew: ['', Validators.required],
      lastNameNew: ['', Validators.required],
      passwordNew: ['', Validators.required],
      passwordRepeatNew: ['', Validators.required],
      rolNew: ['', Validators.required],
      imageUser: ['']
    });
    this.editUserForm = this.formBuilder.group({
      userNameEdit: ['', Validators.required],
      userFirstNameEdit: ['', Validators.required],
      lastNameEdit: ['', Validators.required],
      passwordEdit: [''],
      passwordRepeatEdit: [''],
      rolEdit: ['', Validators.required],
      imageUserEdit: ['']
    });
    this.getUsersTable();
  }

  newUser() {
    this.newUserSubmitted = true;
    if (this.newUserForm.invalid) {
      return false;
    }

    let NewUser: string = this.newUserForm.get("userNameNew").value;
    let NewFirstName: string = this.newUserForm.get("userFirstNameNew").value;
    let NewLastName: string = this.newUserForm.get("lastNameNew").value;
    let passwordNew = this.newUserForm.get("passwordNew").value;
    let passwordRepeatNew = this.newUserForm.get("passwordRepeatNew").value;
    let rol = this.newUserForm.get("rolNew").value;
    let src = document.getElementById("user_photo").getAttribute("src");

    if (passwordRepeatNew != passwordNew) {
      api.Message(MessageType.Error, "Las Contraseñas no coinciden", "Debe repetir la contraseña para continuar");
      return false;
    }

    let nUser = new User({ firstName: NewFirstName, lastName: NewLastName, username: NewUser, password: passwordNew, avatar: src, profile: rol });

    this.loader.show();

    this.userService.AddUser(nUser).subscribe((success) => {
      api.Message(MessageType.Success, "Cuenta Creada", String.Format("La cuenta de {0} ha sido creada satisfactoriamente.", nUser.firstName + " " + nUser.lastName));
      jQuery("#new_user").modal("hide");
      this.newUserForm.reset();
      document.getElementById("user_photo").setAttribute('src', "../../../assets/images/default_user.png");
      this.getUsersTable();
      this.loader.hidde();
    }, (error => { this.PosibleUsersErrors(error); }));
    this.newUserSubmitted = false;
    return false;
  }



  editUser() {
    this.editUserSubmitted = true;
    if (this.editUserForm.invalid) {
      return false;
    }

    this.selected.firstName = this.editUserForm.get("userFirstNameEdit").value;
    this.selected.lastName = this.editUserForm.get("lastNameEdit").value;
    let password = this.editUserForm.get("passwordEdit").value;
    let passwordRepeat = this.editUserForm.get("passwordRepeatEdit").value;
    this.selected.profile = this.editUserForm.get("rolEdit").value;
    this.selected.avatar = document.getElementById("photoEdit").getAttribute("src");

    if (password != null && password != "" && passwordRepeat != password) {
      api.Message(MessageType.Error, "Las Contraseñas no coinciden", "Debe repetir la contraseña para continuar");
      return false;
    }
    if (password != null && password != "") {
      this.selected.password = password;
    }
    this.loader.show();

    this.userService.Update(this.selected).subscribe((success) => {
      api.Message(MessageType.Success, "Cuenta Actualizada", String.Format("La cuenta de {0} ha sido actualizada satisfactoriamente.", this.selected.firstName + " " + this.selected.lastName));
      jQuery("#edit_user").modal("hide");
      this.editUserForm.reset();
      document.getElementById("photoEdit").setAttribute('src', "../../../assets/images/default_user.png");
      this.getUsersTable();
      this.loader.hidde();
    }, (error => { this.PosibleUsersErrors(error); }));
    this.editUserSubmitted = false;
    return false;
  }

  getBase64(file: any) {
    let reader = new FileReader();
    let _file = file.files[0];
    if (_file.type == "image/jpeg" || _file.type == "image/jpg" || _file.type == "image/png") {
      reader.readAsDataURL(_file);
      reader.onload = function () {
        let img = new Image();
        img.onload = function (event) {
          let loadedImage: any = event.currentTarget;
          if (loadedImage.height > max_height_img_allowed || loadedImage.width > max_width_img_allowed)
            api.Message(MessageType.Error, "La Imagen muy Grande", "La máxima resolución es " + max_height_img_allowed + "x" + max_width_img_allowed + ".");
          else {
            document.getElementById("photo").setAttribute('src', img.src);
            document.getElementById("selectedAvatar").innerHTML = _file.name
          }
        };
        img.src = reader.result.toString();
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error, "Error Cargando la Imagen", "Ha ocurrido un error leyendo la imagen, intente nuevamente.");
      };
    }
    else
      api.Message(MessageType.Error, "Formato no soportado", "Solo se permiten los formatos JPEG,JPG or PNG.");
    file.value = ""
  }

  getBase64Edit(file: any) {
    let reader = new FileReader();
    let _file = file.files[0];
    if (_file.type == "image/jpeg" || _file.type == "image/jpg" || _file.type == "image/png") {
      reader.readAsDataURL(_file);
      reader.onload = function () {
        let img = new Image();
        img.onload = function (event) {
          let loadedImage: any = event.currentTarget;
          if (loadedImage.height > max_height_img_allowed || loadedImage.width > max_width_img_allowed)
            api.Message(MessageType.Error, "La Imagen muy Grande", "La máxima resolución es " + max_height_img_allowed + "x" + max_width_img_allowed + ".");
          else {
            document.getElementById("photoEdit").setAttribute('src', img.src);
            document.getElementById("selectedAvatarEdit").innerHTML = _file.name
          }
        };
        img.src = reader.result.toString();
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error, "Error Cargando la Imagen", "Ha ocurrido un error leyendo la imagen, intente nuevamente.");
      };
    }
    else
      api.Message(MessageType.Error, "Formato no soportado", "Solo se permiten los formatos JPEG,JPG or PNG.");
    file.value = ""
  }


  getUsersTable() {
    this.loader.show();
    this.userService.getUsers(this.page, this.maxPerPage, this.filterId, this.filterUser, this.filterFirstName, this.filterLastName).subscribe((data) => {
      this.users = data.body;
      this.users.forEach(element => {
        element.createdAtFormatted = formatDate(new Date(element.createdAt), "yyyy-MM-dd hh:mm a", "en-US");
      });
      this.total = +data.headers.get("X-Total-Count");
      this.loader.hidde();
    },
      error => { this.PosibleUsersErrors(error); }
    );
  }



  changeStatus(item: User) {
    this.loader.show();
    this.userService.changeUserStatus(item.id.toString()).subscribe((data) => {
      this.loader.hidde();
      this.getUsersTable();
    },
      error => { this.PosibleUsersErrors(error); }
    );
  }

  setfilterUserId(filter: string) {
    this.filterId = filter;
    this.getUsersTable();
  }

  setfilterFirstName(filter: string) {
    this.filterFirstName = filter;
    this.getUsersTable();
  }

  setfilterLastName(filter: string) {
    this.filterLastName = filter;
    this.getUsersTable();
  }

  setfilterUserNameFilter(filter: string) {
    this.filterUser = filter;
    this.getUsersTable();
  }

  select(item: User) {
    this.selected = item;
    this.editUserForm.get("userNameEdit").setValue(this.selected.username);
    this.editUserForm.get("userFirstNameEdit").setValue(this.selected.firstName);
    this.editUserForm.get("lastNameEdit").setValue(this.selected.lastName);
    this.editUserForm.get("rolEdit").setValue(this.selected.profile);
    this.editUserForm.get("passwordEdit").setValue("");
    this.editUserForm.get("passwordRepeatEdit").setValue("");
    document.getElementById("photoEdit").setAttribute("src", this.selected.avatar);

  }



  PosibleUsersErrors(error: HttpErrorResponse) {
    this.loader.hidde();
    switch (error.status) {
      case 401:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Ha expirado su sessión", "Necesita acceder nuevamente.");
        break;
      case 403:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Falta de Permisos", "Usted no tiene permisos para realizar esa acción.");
        break;
      case 409:
        api.Message(MessageType.Error, "Ya existe esa Cuenta", "La cuenta que está intentando crear ya existe.");
        break;
      default:
        api.Message(MessageType.Error, String.Format("Error Desconocido (Código:{0})", error.status), error.message);
        break;
    }
  }



}
