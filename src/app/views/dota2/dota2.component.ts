import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import { HeroesService } from 'src/app/services/heroes/heroes.service';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { MatchService } from 'src/app/services/match/match.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { HttpErrorResponse } from '@angular/common/http';
import { API, MessageType } from 'src/app/services/api-global';
import { String, StringBuilder } from 'typescript-string-operations';
import { StadisticsService } from 'src/app/services/stadistics/stadistics.service';
import { UserSumary } from 'src/app/services/stadistics/UserSumary';

const api = new API();

@Component({
  selector: 'app-dota2',
  templateUrl: './dota2.component.html',
  styleUrls: ['./dota2.component.css']
})
export class Dota2Component implements OnInit {

  public wins_losses: ChartDataSets[] = [   
  ];
 
  

  public matchsWins: ChartDataSets[] = [{label:"",data:[]}];
  public matchs: Label[] = [];
  loadingMatchsWins:boolean = true;

  public players: Label[] = [''];
  public playersWins: number[] = [0];
  loadingPlayersWins:boolean = true;

  public playersL: Label[] = [''];
  public playersLose: number[] = [0];
  loadingPlayersLose:boolean = true;

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  public barChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(100,250,100,0.8)',
      borderColor: 'rgba(50,250,50,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // red
      backgroundColor: 'rgba(250,100,100,0.8)',
      borderColor: 'rgba(250,50,50,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
 
  public pieChartPlugins = [];  

  public winsChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {

      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [

      ],
    },
  };

  public winsColors: Color[] = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public winsChartPlugins = [pluginAnnotations];

  userTable:UserSumary[] = [];

 ngOnInit() {
  this.loadUserTable();
  this.loadPlayersWins();
 } 

 constructor (private stadistics:StadisticsService,private userService: UsersService, private heroService: HeroesService, private loader: LoaderService, private matchService: MatchService, private authentication: AuthenticationService){
 } 

 loadPlayersWins(){
   this.loadingPlayersWins = true;
   this.stadistics.totalWins().subscribe(data=>{
    this.players = data.map(x=>x.label);
    this.playersWins = data.map(x=>x.value);
    this.wins_losses = [];
    this.wins_losses.push({data:data.map(x=>x.value),label:"Ganados"});
    this.loadingPlayersWins = false;
    this.loadPlayersLose();
   },
   error=>{
     this.PosibleErrors(error);
     this.loadingPlayersWins = false;
   });
 }

 loadUserTable(){
   //this.loader.show();
   this.stadistics.getUserTable().subscribe(data=>{
     this.userTable = data;
   },error=>{this.PosibleErrors(error);});
 }

 loadPlayersLose(){
  this.loadingPlayersLose = true;
  this.stadistics.totalLose().subscribe(data=>{
   this.playersL = data.map(x=>x.label);
   this.playersLose = data.map(x=>x.value);   
   this.wins_losses.push({data:data.map(x=>x.value),label:"Perdidos"});
   this.loadingPlayersLose = false;
   this.loadWinsPerDate();
  },
  error=>{
    this.PosibleErrors(error);
    this.loadingPlayersLose = false;
  });
}

 loadWinsPerDate(){
   this.loadingMatchsWins = true;
   this.stadistics.winsPerDate().subscribe(result=>{     
     this.matchs = result.xLabels;   
     this.matchsWins = [];    
     result.values.forEach(element => {
      this.matchsWins.push({data:element.data,label:element.label});
     });        
     this.loadingMatchsWins = false;    
   },error=>{
     this.PosibleErrors(error);
     this.loadingMatchsWins = false;
    });
 }

 PosibleErrors(error: HttpErrorResponse) {
  this.loader.hidde();
  switch (error.status) {
    case 0:
        api.Message(MessageType.Error,"Existen Problemas Técnicos","En estos momentos el servidor de Datos no está disponible, contacte a su administrador para las dudas, intente un poco más tarde.");         
    break;
    case 401:
      this.authentication.logOut();
      api.Message(MessageType.Error, "Ha expirado su sessión", "Necesita acceder nuevamente.");
      break;
    case 403:
      this.authentication.logOut();
      api.Message(MessageType.Error, "Falta de Permisos", "Usted no tiene permisos para realizar esa acción.");
      break;   
    default:
      api.Message(MessageType.Error, String.Format("Error Desconocido (Código:{0})", error.status), error.message);
      break;
  }
}
}
