import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { API, MessageType } from 'src/app/services/api-global';
import { forEach } from '@angular/router/src/utils/collection';
import { element } from '@angular/core/src/render3';
import { String, StringBuilder } from 'typescript-string-operations';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { Settings } from 'src/app/services/settings/Settings';


const api = new API();
declare var jQuery: any;
let loadLang = null;

const max_height_img_allowed = 512;
const max_width_img_allowed = 512;

let toBigImgHeader = "Image to big";
let toBigImgMSG = "Higest resolution allowed is " + max_width_img_allowed + "x" + max_height_img_allowed;
let errorLoadingImageHeader = "Error loading image";
let errorLoadingImageMSG = "Error loading image, the file is corrupt";
let formatError = "Image format not allowed";
let formatErrorMSG = "Only PNG, JPG or JPEG formats are allowed";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {   
  changeLangForm: FormGroup;
   

  constructor(public translate: TranslateService, private formBuilder: FormBuilder, private settingsService: SettingsService, private loader: LoaderService, private authentication: AuthenticationService) {

  }
  
  ngOnInit() {    
    this.changeLangForm = this.formBuilder.group(
      {
        language: [''],
        companyCountry: ['']
      });   

    if (localStorage.getItem("LANG") == null) {
      const browserLang = this.translate.getBrowserLang();
      this.changeLangForm.get("language").setValue(browserLang);
    }
    else {
      this.changeLangForm.get("language").setValue(localStorage.getItem("LANG"));
    }
  }  

  changeLanguage() {
    this.translate.use(this.changeLangForm.get("language").value);
    localStorage.setItem("LANG", this.changeLangForm.get("language").value);
    return false;
  } 
}


