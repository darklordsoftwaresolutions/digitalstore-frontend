import { Component, OnInit } from '@angular/core';
import { Tournament } from 'src/app/services/tournament/Tournament';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { UsersService } from 'src/app/services/users/users.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { API, MessageType } from 'src/app/services/api-global';
import { TournamentService } from 'src/app/services/tournament/tournament.service';
import { HttpErrorResponse } from '@angular/common/http';
import { String, StringBuilder } from 'typescript-string-operations';
import { formatDate } from '@angular/common';

const api = new API();
const max_height_img_allowed = 1024;
const max_width_img_allowed = 1024;
declare var jQuery: any;

@Component({
  selector: 'app-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.css']
})
export class TournamentsComponent implements OnInit {

  items: Tournament[] = [];
  selected: Tournament = new Tournament();
  total: number = 0;
  page: number = 1;
  maxPerPage: number = 10;
  filterId: string = "";  
  filterName: string = "";
  filterByStatus:string;
  filterByType:string;
  filterStartDate: string = "";
  filterEndDate: string = "";

  newForm: FormGroup;
  newSubmitted: boolean = false;
  get fnt() { return this.newForm.controls; }

  editForm: FormGroup;
  editSubmitted: boolean = false;
  get fet() { return this.editForm.controls; }

  constructor(private loader: LoaderService, private tournamentService: TournamentService, private authentication: AuthenticationService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.newForm = this.formBuilder.group({
      NameNew: ['', Validators.required],
      imageNew: [''],
      statusNew: ['', Validators.required],
      typeNew: ['', Validators.required]     
    });
    this.editForm = this.formBuilder.group({
      NameEdit: ['', Validators.required],
      imageEdit: [''],
      statusEdit: ['', Validators.required],
      typeEdit: ['', Validators.required]     
    });
    this.getItems();
  }

  getItems(){
    this.loader.show();
    this.tournamentService.getAll(this.page,this.maxPerPage,this.filterId,this.filterName,this.filterByStatus,this.filterByType,this.filterStartDate,this.filterEndDate).subscribe(data=>{
      this.items = data.body;
      this.total = +data.headers.get("X-Total-Count");
      this.items.forEach(element => {
        element.createdAtFormatted = formatDate(new Date(element.createdAt), "yyyy-MM-dd hh:mm a", "en-US");
      });
      this.loader.hidde();
    },error=>{
      this.PosibleTournamentsErrors(error);
    });
    
  }

  getBase64(file: any) {
    let reader = new FileReader();
    let _file = file.files[0];
    if (_file.type == "image/jpeg" || _file.type == "image/jpg" || _file.type == "image/png") {
      reader.readAsDataURL(_file);
      reader.onload = function () {
        let img = new Image();
        img.onload = function (event) {
          let loadedImage: any = event.currentTarget;
          if (loadedImage.height > max_height_img_allowed || loadedImage.width > max_width_img_allowed)
            api.Message(MessageType.Error, "La Imagen muy Grande", "La máxima resolución es " + max_height_img_allowed + "x" + max_width_img_allowed + ".");
          else {
            document.getElementById("photo").setAttribute('src', img.src);
            document.getElementById("selectedAvatarNew").innerHTML = _file.name
          }
        };
        img.src = reader.result.toString();
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error, "Error Cargando la Imagen", "Ha ocurrido un error leyendo la imagen, intente nuevamente.");
      };
    }
    else
      api.Message(MessageType.Error, "Formato no soportado", "Solo se permiten los formatos JPEG,JPG or PNG.");
    file.value = ""
  }

  getBase64Edit(file: any) {
    let reader = new FileReader();
    let _file = file.files[0];
    if (_file.type == "image/jpeg" || _file.type == "image/jpg" || _file.type == "image/png") {
      reader.readAsDataURL(_file);
      reader.onload = function () {
        let img = new Image();
        img.onload = function (event) {
          let loadedImage: any = event.currentTarget;
          if (loadedImage.height > max_height_img_allowed || loadedImage.width > max_width_img_allowed)
            api.Message(MessageType.Error, "La Imagen muy Grande", "La máxima resolución es " + max_height_img_allowed + "x" + max_width_img_allowed + ".");
          else {
            document.getElementById("photoEdit").setAttribute('src', img.src);
            document.getElementById("selectedAvatarEdit").innerHTML = _file.name
          }
        };
        img.src = reader.result.toString();
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error, "Error Cargando la Imagen", "Ha ocurrido un error leyendo la imagen, intente nuevamente.");
      };
    }
    else
      api.Message(MessageType.Error, "Formato no soportado", "Solo se permiten los formatos JPEG,JPG or PNG.");
    file.value = ""
  }

  newItem(){
    this.newSubmitted = true;
    if (this.newForm.invalid) {
      return false;
    }

    let name: string = this.newForm.get("NameNew").value;
    let type:string = this.newForm.get("typeNew").value;    
    let status:string = this.newForm.get("statusNew").value;   
    let src = document.getElementById("user_photo").getAttribute("src");

    
    this.loader.show();

    this.tournamentService.add(new Tournament({name:name,type:+type,status:+status,photo:src})).subscribe((success) => {
      api.Message(MessageType.Success, "Torneo Creado", String.Format("El Torneo {0} ha sido creada satisfactoriamente.", name));
      jQuery("#new_item").modal("hide");
      this.newForm.reset();
      document.getElementById("photo").setAttribute('src', "../../../assets/images/default_logo.png");
      this.getItems();
      this.loader.hidde();
    }, (error => { this.PosibleTournamentsErrors(error); }));
    this.newSubmitted = false;
    return false;
  }

  PosibleTournamentsErrors(error: HttpErrorResponse) {
    this.loader.hidde();
    switch (error.status) {
      case 401:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Ha expirado su sessión", "Necesita acceder nuevamente.");
        break;
      case 403:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Falta de Permisos", "Usted no tiene permisos para realizar esa acción.");
        break;
      case 409:
        api.Message(MessageType.Error, "Ya existe ese Torneo", "El Torneo que está intentando crear ya existe.");
        break;
      default:
        api.Message(MessageType.Error, String.Format("Error Desconocido (Código:{0})", error.status.toString()), error.message);
        break;
    }
  }
}
