import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { String, StringBuilder } from 'typescript-string-operations';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { API, MessageType, downloadList } from '../../services/api-global';
import { LoaderService } from '../../services/loader/loader.service';
import { User } from '../../services/users/User';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UsersService } from 'src/app/services/users/users.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DirectionDownloadStatus } from '../template/DownloadStatus';

const api = new API();
const max_height_img_allowed = 1024;
const max_width_img_allowed = 1024;
declare var jQuery:any;



@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  public activeLang = 'en';
  userLogin: User = new User({avatar:"../../../assets/images/default_user.png"});  
  path:string="";
  language_list = [{'name': 'english', 'url': 'https://raw.githubusercontent.com/stevenrskelton/flag-icon/master/png/16/country-4x3/gb.png'},{'name': 'italian', 'url': 'https://raw.githubusercontent.com/stevenrskelton/flag-icon/master/png/16/country-4x3/it.png'}];
  selected:number = 0;
  themeMenu:string = "pcoded-navbar menu-light brand-lightblue icon-colored";
  themeHeader:string = "navbar pcoded-header navbar-expand-lg navbar-light header-lightblue headerpos-fixed";
  theme:string = "light";
  facilityName:string = "(-)";

  editProfileForm: FormGroup = this.formBuilder.group({
    userNameEditProfile:['',Validators.required],
    userFirstNameProfile:['',Validators.required],
    lastNameProfile:['',Validators.required],
    passwordOldProfile:['',Validators.required],
    passwordProfile:[''],
    passwordRepeatProfile:['']   
  });
  ProfileUserSubmitted:boolean = false;
  get feu() {return this.editProfileForm.controls;}
  get downloadList(){ return downloadList;}
  get status(){return DirectionDownloadStatus};

  activeMenu(sel:number)
  {
    this.selected= sel;    
  }

  constructor(private userService: UsersService,private formBuilder: FormBuilder,private authService: AuthenticationService,private loader: LoaderService, public translate: TranslateService,private titleService: Title) {
    if(this.authService.verifyAccess())
    {      
      this.userLogin = this.authService.loadSession();  
    }      
    translate.addLangs(['en','es','fr']);
    translate.setDefaultLang('es');
    let browserLang = translate.getBrowserLang();
    if(localStorage.getItem("LANG")!=null) 
      browserLang = localStorage.getItem("LANG");    
    else
      localStorage.setItem("LANG",browserLang);    
    translate.use(browserLang.match("en|fr|es") ? browserLang : 'es');
  }

  ngOnInit() {
    this.userService.getUserById(this.userLogin.id.toString()).subscribe((data=>{
      this.userLogin = data;
      document.getElementById("photoProfile").setAttribute('src',this.userLogin.avatar); 
      this.editProfileForm.get("userNameEditProfile").setValue(this.userLogin.username);
      this.editProfileForm.get("userFirstNameProfile").setValue(this.userLogin.firstName);
      this.editProfileForm.get("lastNameProfile").setValue(this.userLogin.lastName);
    }));
    this.translate.get('TITLE').subscribe((translated: string) => {
      this.titleService.setTitle(translated);
    if(this.InPath("/dashboard"))    
    {
      this.selected=0;
      this.path="Dashboard"
    }
    if(this.InPath("/users"))
    {
      this.selected=1;
      this.path="Usuarios";
    }      
    if(this.InPath("/settings"))
    {
      this.selected=5;
      this.path="Opciones";
    }
    if(this.InPath("/logs"))
    {
      this.selected=6;
      this.path="Logs";
    }
    if(this.InPath("/match"))
    {
      this.selected=4;
      this.path="Partidos";
    }
    if(this.InPath("/dota2")){
      this.selected=2;
      this.path="Dota 2 Sala Principal";
    }   
    if(this.InPath("/heroes")){
      this.selected=3;
      this.path="Héroes";
    } 

    this.authService.loadSession();
   
    this.loadingComanySettings();
    
  });    
  }

  ProfileUser(){
    this.ProfileUserSubmitted =  true;
    if(this.editProfileForm.invalid)
    {      
      return false;
    }   
    let user:User = new User({id:this.userLogin.id,username:this.userLogin.username});    
    user.password = this.editProfileForm.get("passwordOldProfile").value;   
    if(user.password!=this.userLogin.password){
      api.Message(MessageType.Error,"Contraseña Incorrecta","Debe escribir su contraseña para aplicar los cambios.");
      return false;
    }
    user.firstName = this.editProfileForm.get("userFirstNameProfile").value; 
    user.lastName = this.editProfileForm.get("lastNameProfile").value;   

    let newPassword = this.editProfileForm.get("passwordProfile").value;
    let passwordRepeat = this.editProfileForm.get("passwordRepeatProfile").value;

    user.avatar = document.getElementById("photoProfile").getAttribute("src");

    if(passwordRepeat!=newPassword){
      api.Message(MessageType.Error,"Las Contraseñas no coinciden","Debe repetir la contraseña para continuar");
      return false;
    }   
    user.password = newPassword!="" && newPassword!=undefined ? newPassword : user.password; 
    this.loader.show();
    
    this.userService.UpdateProfile(user).subscribe((success)=>
      {        
        this.userLogin = user;
        api.Message(MessageType.Success,"Cuenta Actualizada",String.Format("Su Perfil de Usuario ({0}) ha sido actualizado correctamente.",this.userLogin.firstName+" "+this.userLogin.lastName));                
        jQuery("#edit_profile").modal("hide");               
        this.loader.hidde(); 
      },(error=>{this.PosibleUsersErrors(error);}));
    this.ProfileUserSubmitted =  false;   
    return false;  
  }

  getBase64(file:any) 
  {  
    let reader = new FileReader();
    let _file = file.files[0];    
    if(_file.type=="image/jpeg" || _file.type=="image/jpg" || _file.type=="image/png" )
    {      
      reader.readAsDataURL(_file);
      reader.onload = function() {
        let img = new Image();
        img.onload = function (event) 
        {
          let loadedImage: any = event.currentTarget;          
          if(loadedImage.height>max_height_img_allowed||loadedImage.width>max_width_img_allowed)
            api.Message(MessageType.Error,"La Imagen muy Grande","La máxima resolución es "+max_height_img_allowed+"x"+max_width_img_allowed+".");
          else
          { 
            document.getElementById("photoProfile").setAttribute('src',img.src);            
            document.getElementById("avatarProfile").innerHTML = _file.name
          }             
        };
        img.src = reader.result.toString();   
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error,"Error Cargando la Imagen","Ha ocurrido un error leyendo la imagen, intente nuevamente.");
      };
    }
    else
       api.Message(MessageType.Error,"Formato no soportado","Solo se permiten los formatos JPEG,JPG or PNG.");
    file.value=""
  }

  loadingComanySettings()
  {
      
  }

  setUserLogin(user)
  {
    this.userLogin = user;
  }

  LogOut()
  {
    this.authService.logOut();
  }

  InPath(path:string)
  {    
    return window.location.href.includes(path);
  }

  PosibleUsersErrors(error:HttpErrorResponse)
  {
    this.loader.hidde();
    switch(error.status)
       {         
        case 401:          
        this.authService.logOut();   
        api.Message(MessageType.Error,"Ha expirado su sessión","Necesita acceder nuevamente.");     
        break;
        case 403:
        api.Message(MessageType.Error,"La api es inaccesible","Verifique su conexión o contacte al administrador.");         
        break;       
        default:
        api.Message(MessageType.Error,String.Format("Error Desconocido (Código:{0})",error.status),error.message);        
        break; 
       }       
  }

}
