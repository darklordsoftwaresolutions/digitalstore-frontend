import { DirectionDownloadStatus } from './DownloadStatus';
export class DownloadItem
{
    public constructor(init?:Partial<DownloadItem>) {
        Object.assign(this, init);
    }   
    fromUrl:string;
    bytesDownloaded:number;
    totalBytes:number;
    percent:number;
    fileName:string;
    status:DirectionDownloadStatus;     
    
  }