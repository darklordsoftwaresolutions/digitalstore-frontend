export enum DirectionDownloadStatus {
    InProgress,
    Canceled,
    Error,
    Pause,
    Complete,
}