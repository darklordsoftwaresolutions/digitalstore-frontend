import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TemplateComponent } from '../template.component';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { SettingsComponent } from '../../settings/settings.component';
import { AnalyticsComponent } from '../../analytics/analytics.component';
import { UsersComponent } from '../../users/users.component';
import { TournamentsComponent } from '../../tournaments/tournaments.component';
import { Dota2Component } from '../../dota2/dota2.component';
import { HeroesComponent } from '../../heroes/heroes.component';
import { LogsComponent } from '../../logs/logs.component';
import { MatchComponent } from '../../match/match.component';



const routes: Routes = [
  {
    path: '',
    component: TemplateComponent,
    children: [
        {
          path: 'dashboard',
          component: DashboardComponent
        },        
        {
          path: 'users',
          component: UsersComponent
        },    
        {
          path: 'dota2',
          component: Dota2Component
        },    
        {
          path: 'heroes',
          component: HeroesComponent
        }, 
        {
          path: 'settings',
          component: SettingsComponent
        },
        {
          path: 'match',
          component: MatchComponent
        },
        {
          path: 'logs',
          component: LogsComponent
        }

    ]
}  
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateRoutingModule { }
