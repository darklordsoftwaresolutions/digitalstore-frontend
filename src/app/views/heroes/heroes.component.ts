import { Component, OnInit } from '@angular/core';
import { Heroe } from 'src/app/services/heroes/Heroes';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { HeroesService } from 'src/app/services/heroes/heroes.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { API, MessageType } from 'src/app/services/api-global';
import { HttpErrorResponse } from '@angular/common/http';
import { String, StringBuilder } from 'typescript-string-operations';
import { error } from 'protractor';

const api = new API();
const max_size_allowed = 1024*1024*10;
declare var jQuery: any;

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  filterName:string = "";
  filterType:number = -1;
  listStrength:Heroe[] = [];
  totalStrength:number = 0;
  loadingS:boolean = true;
  listAgility:Heroe[] = [];
  totalAgility:number = 0;
  loadingA:boolean = true;
  listIntelligence:Heroe[] = [];
  totalIntelligence:number = 0;
  loadingI:boolean = true;

  pageS = 1;
  pageA = 1;
  pageI = 1;
  maxPerPage = 1;

  selected:Heroe = new Heroe();

  newHeroForm: FormGroup;
  newHeroSubmitted: boolean = false;

  editHeroForm: FormGroup;
  editHeroSubmitted: boolean = false;

  pI:number = 0;
  pS:number = 0;
  pA:number = 0;

  get fnh() { return this.newHeroForm.controls; }
  get feh() { return this.editHeroForm.controls; } 

  constructor(private formBuilder: FormBuilder,private loader: LoaderService,private heroService:HeroesService,private authentication: AuthenticationService) { }

  ngOnInit() {
    this.newHeroForm = this.formBuilder.group({
      Name: ['', Validators.required],
      Type: ['0', Validators.required],      
      imageHero: ['', Validators.required]
    });
    this.editHeroForm = this.formBuilder.group({
      Name: ['', Validators.required],
      Type: ['0', Validators.required],      
      imageHero: ['',]
    });
    this.getHeroes();
  }

  loadMoreStrength(){
    if(this.listStrength.length<this.totalStrength){
      this.pageS++;
      this.heroService.get(this.pageS,this.maxPerPage,this.filterName,0).subscribe(strength =>{
        this.listStrength = this.listStrength.concat(strength.body);  
        this.pS = (this.listStrength.length*100)/this.totalStrength;      
        this.loadMoreStrength();
      },error=>{this.PosibleErrors(error);this.loadingS = false;});
    }
    else{
      this.pS = 100;
      this.loadingS = false;
    }
      
  }

  loadMoreAgility(){
    if(this.listAgility.length<this.totalAgility){

      this.pageA++;
      this.heroService.get(this.pageA,this.maxPerPage,this.filterName,1).subscribe(agility =>{
        this.listAgility = this.listAgility.concat(agility.body);
        this.pA = (this.listAgility.length*100)/this.totalAgility;
        this.loadMoreAgility();
      },error=>{this.PosibleErrors(error);this.loadingA = false;});
    }
    else{
      this.pA = 100;
      this.loadingA = false;
    }
      
  }

  loadMoreIntelligence(){
    if(this.listIntelligence.length<this.totalIntelligence){
      this.pageI+=1;
      this.heroService.get(this.pageI,this.maxPerPage,this.filterName,2).subscribe(intelligence =>{        
        this.listIntelligence = this.listIntelligence.concat(intelligence.body);  
        this.pI = (this.listIntelligence.length*100)/this.totalIntelligence;    
        this.loadMoreIntelligence();
      },error=>{this.PosibleErrors(error);this.loadingI = false;});
    }
    else{
      this.pI = 100;
      this.loadingI = false;
    }
      
  }

  delPreview(){
    jQuery("#del_hero").modal("show");
  }

  deleteHero(){
    this.loader.show();
    this.heroService.delete(this.selected.id.toString()).subscribe(data=>{
      api.Message(MessageType.Success, "Héroe Eliminado", String.Format("Se ha eliminado el héroe {0} correctamente.", this.selected.name));
      this.getHeroes();
      this.loader.hidde();
    },error=>this.PosibleErrors(error));
  }

  getHeroes(){
    this.loadingS = true;
    this.pageS = 1;
    this.pS = 0;
    this.heroService.get(this.pageS,this.maxPerPage,this.filterName,0).subscribe(strength =>{
      this.listStrength = strength.body;
      this.totalStrength = +strength.headers.get("X-Total-Count");
      this.loadMoreStrength();
    },error=>{this.PosibleErrors(error);this.loadingS = false;});
    this.loadingA = true;
    this.pageA  = 1;
    this.pA = 0;
    this.heroService.get(this.pageA,this.maxPerPage,this.filterName,1).subscribe(agility =>{
      this.listAgility = agility.body;
      this.totalAgility = +agility.headers.get("X-Total-Count");
      this.loadMoreAgility();
    },error=>{this.PosibleErrors(error);this.loadingA = false;});
    this.loadingI = true;
    this.pageI = 1;
    this.pI = 0;
    this.heroService.get(this.pageI,this.maxPerPage,this.filterName,2).subscribe(intelligence =>{
      this.listIntelligence = intelligence.body;
      this.totalIntelligence = +intelligence.headers.get("X-Total-Count");
      this.loadMoreIntelligence();
    },error=>{this.PosibleErrors(error);this.loadingI = false;});
  }

  selectHero(item:Heroe){
    this.selected = item;
    this.editHeroForm.get("Name").setValue(this.selected.name);
    this.editHeroForm.get("Type").setValue(this.selected.type);    
    document.getElementById("photo").setAttribute("src",this.selected.photo);
  }

  newHero() {
    this.newHeroSubmitted = true;
    if (this.newHeroForm.invalid) {
      return false;
    }

    let Name: string = this.newHeroForm.get("Name").value;
    let Type: number = +this.newHeroForm.get("Type").value;    
    let src:string = document.getElementById("photo").getAttribute("src");

    this.loader.show();

    this.heroService.add(new Heroe({name:Name,type:Type,photo:src})).subscribe((success) => {
      api.Message(MessageType.Success, "Héroe Creado", String.Format("Se ha creado el héroe {0} correctamente.", Name));
      jQuery("#new_hero").modal("hide");
      this.newHeroForm.reset();
      document.getElementById("photo").setAttribute('src', "../../../assets/images/pods_bg.webm");
      document.getElementById("selectedHero").innerHTML = "Seleccionar Imagen del Héroe";
      this.getHeroes();
      this.loader.hidde();
    }, (error => { this.PosibleErrors(error); }));
    this.newHeroSubmitted = false;
    return false;
  }

  editHero() {
    this.editHeroSubmitted = true;
    if (this.editHeroForm.invalid) {
      return false;
    }

    this.selected.name= this.editHeroForm.get("Name").value;
    this.selected.type = +this.editHeroForm.get("Type").value;    
    this.selected.photo = document.getElementById("photoEdit").getAttribute("src");    

    this.loader.show();

    this.heroService.update(this.selected).subscribe((success) => {
      api.Message(MessageType.Success, "Héroe Actualizado", String.Format("Se ha actualizado el héroe {0} correctamente.", this.selected.name));  
      this.loader.hidde();
    }, (error => { this.PosibleErrors(error); }));
    this.editHeroSubmitted = false;
    return false;
  }

  setNameFilter(filter:string){
    this.filterName = filter;
    this.getHeroes();
  }

  setTypeFilter(filter:number){
    this.filterType = filter;
    this.getHeroes();
  }

  getBase64(file: any) {
    let reader = new FileReader();  
    let _file = file.files[0];    
    if (_file.type == "video/webm") {
      reader.readAsDataURL(_file);      
      reader.onload = function () { 
        let data:string = reader.result.toString();
        max_size_allowed
        if(data.length>max_size_allowed){
          api.Message(MessageType.Error, "Fichero demasiado Grande", "El fichero excede los limites permitidos: "+max_size_allowed/1024+" Kb");
          return;
        }
        document.getElementById("photo").setAttribute('src', data);
        document.getElementById("selectedHero").innerHTML = _file.name
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error, "Error Cargando el Fichero", "Ha ocurrido un error leyendo el fichero, intente nuevamente.");
        return;
      };
    }
    else
      api.Message(MessageType.Error, "Formato no soportado", "Solo se permite lel formato WEBM.");
    file.value = ""
  }

  getBase64Edit(file: any) {
    let reader = new FileReader();   
    let _file = file.files[0];    
    if (_file.type == "video/webm") {
      reader.readAsDataURL(_file);      
      reader.onload = function () { 
        let data:string = reader.result.toString();
        max_size_allowed
        if(data.length>max_size_allowed){
          api.Message(MessageType.Error, "Fichero demasiado Grande", "El fichero excede los limites permitidos: "+max_size_allowed/1024+" Kb");
          return;
        }
        document.getElementById("photoEdit").setAttribute('src', data);
        document.getElementById("selectedHeroEdit").innerHTML = _file.name
      };
      reader.onerror = function (error) {
        api.Message(MessageType.Error, "Error Cargando el Fichero", "Ha ocurrido un error leyendo el fichero, intente nuevamente.");
        return;
      };
    }
    else
      api.Message(MessageType.Error, "Formato no soportado", "Solo se permite lel formato WEBM.");
    file.value = ""
  }

  PosibleErrors(error: HttpErrorResponse) {
    this.loader.hidde();
    switch (error.status) {
      case 0:
          api.Message(MessageType.Error,"Existen Problemas Técnicos","En estos momentos el servidor de Datos no está disponible, contacte a su administrador para las dudas, intente un poco más tarde.");         
      break;
      case 401:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Ha expirado su sessión", "Necesita acceder nuevamente.");
        break;
      case 403:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Falta de Permisos", "Usted no tiene permisos para realizar esa acción.");
        break;
      case 409:
        api.Message(MessageType.Error, "Ya existe ese Héroe", "El héroe que está intentando crear ya existe.");
        break;
      default:
        api.Message(MessageType.Error, String.Format("Error Desconocido (Código:{0})", error.status), error.message);
        break;
    }
  }

}
