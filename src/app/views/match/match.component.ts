import { Component, OnInit, Output } from '@angular/core';
import { Heroe } from 'src/app/services/heroes/Heroes';
import { formatDate } from '@angular/common';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { MatchService } from 'src/app/services/match/match.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Match } from 'src/app/services/match/Match';
import { API, MessageType, downloadList } from 'src/app/services/api-global';
import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { String, StringBuilder } from 'typescript-string-operations';
import { Player } from 'src/app/services/match/Player';
import { HeroesService } from 'src/app/services/heroes/heroes.service';
import { error, EventEmitter } from 'protractor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Join } from 'src/app/services/match/Join';
import { UsersService } from 'src/app/services/users/users.service';
import { PlayerResult } from 'src/app/services/match/PlayerResult';
import { imageCaption } from '@syncfusion/ej2-angular-richtexteditor';
import { ScoreLogs } from 'src/app/services/match/ScoreLogs';
import { saveAs } from 'file-saver';
import * as FileSaver from 'file-saver';
import { DownloadItem } from '../template/DownloadItem';
import { DirectionDownloadStatus } from '../template/DownloadStatus';
import { User } from 'src/app/services/users/User';


const api = new API();
declare var jQuery: any;

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {

  items: Match[] = [];
  heroes: Heroe[] = [];
  selectedHero: Heroe = new Heroe({ photo: "../../../assets/images/pods_bg.webm" });
  loadingHeroes: boolean = false;
  selected: Match = new Match();
  total: number = 0;
  page: number = 1;
  maxPerPage: number = 10;
  filterType: number = -1;

  killsDire:number = 0;
  killsRadiant:number = 0;
  pointsDire:number = 0;
  pointsRadiant:number =0;

  imgCapture: string = "../../../assets/images/l1.jpg";
  replayFile:File = null;
  replayFileSize: string = "0.00 mb";  
  replayFileDate: string = "-";
  teamWinner:number = -1;
  endMatchSubmitted: boolean = false;
  progress:number = 0;
  userLogin:User = new User({profile:"Client"});
  //@Output() public onUploadFinished = new EventEmitter();

  joinForm: FormGroup;
  joinSubmitted: boolean = false;
  get fj() { return this.joinForm.controls; }

  resultsForm: FormGroup;
  resultsFormSubmitted: boolean = false;
  get fr() { return this.resultsForm.controls; }

  selectedPlayer: Player = new Player({ hero: "-", rol: -1, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });

  player1: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player2: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player3: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player4: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player5: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player6: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player7: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player8: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player9: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
  player10: Player = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });

  date: string = "01/01/1990 00:00:00 am";
  info: string = "Esperando a los Jugadores...";

  intervalUpdate;
  combatLogs:ScoreLogs[]=[];
  selectedPlayerResult:PlayerResult = new PlayerResult();

  filterStatus:string="-1";
  filterFrom:Date = null;
  filterTo:Date = null;

  


  constructor(private formBuilder: FormBuilder, private userService: UsersService, private heroService: HeroesService, private loader: LoaderService, private matchService: MatchService, private authentication: AuthenticationService) { }

  ngOnInit() {
    this.userLogin = this.authentication.loadSession();
    this.joinForm = this.formBuilder.group({
      joinHero: ['', Validators.required],
      joinRol: ['', Validators.required],
      joinTeam: ['', Validators.required]
    });
    this.resultsForm = this.formBuilder.group({
      Level: [1, Validators.required],
      Kills: [0, Validators.required],
      Dies: [0, Validators.required],
      Assist: [0, Validators.required],
      Creeps: [0, Validators.required],
      CreepsDeny: [0, Validators.required],
      Damage: [0],
      DamageTaken: [0],
      DamageBlocked: [0],
      Heal: [0],
      Mana: [0],
      NetValue: [0],
      Wards: [0],
      GracePoint: [0]
    });
    this.loadHeroes();

    setInterval(() => {
      this.date = formatDate(new Date(), "yyyy-MM-dd hh:mm a", "en-US");
    }, 1000);
    this.loadMatchs();
  }

  setIdFilter(filter:string){

  }

  setStatusFilter(StatusFilter:string){
    this.filterStatus = StatusFilter;
    this.loadMatchs();
  }

  setFilterFrom(StartDate:Date){
    this.filterFrom = StartDate;
    this.loadMatchs();
  }

  setFilterTo(EndDate){
    this.filterTo = EndDate;
    this.loadMatchs();
  }

  endMatch(){    
    this.endMatchSubmitted = true;
    if(this.replayFile==null){
      api.Message(MessageType.Error,"Formulario Incompleto","El fichero  del replay es obligatorio.");
      return;
    }
    if(this.imgCapture=="../../../assets/images/l1.jpg"){
      api.Message(MessageType.Error,"Formulario Incompleto","La captura de la partida es obligatoria.");
      return;
    }
    if(this.teamWinner==-1){
      api.Message(MessageType.Error,"Formulario Incompleto","Debe seleccionar el equipo ganador.");
      return;
    }    
    const formData = new FormData();
    formData.append('file', this.replayFile, this.replayFile.name);
    formData.append('capture',this.imgCapture);
    formData.append('id',this.selected.id.toString());
    formData.append('winners',this.teamWinner.toString());

    this.matchService.endMatch(formData).subscribe(data=>{      
      if (data.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * data.loaded / data.total);
        else if (data.type === HttpEventType.Response) {    
          jQuery("#end_match").modal("hide");
          this.loadMatchs();
        }
    },error=>{this.PosibleMatchErrors(error);});

    this.endMatchSubmitted = false;
  }

  loadReplay(replayFile:any){    
    this.replayFile = replayFile.files[0];    
    this.replayFileSize = ((this.replayFile.size)/(1024*1024)).toFixed(2)+" mb";
    this.replayFileDate = formatDate(new Date(this.replayFile.lastModified), "yyyy-MM-dd hh:mm a", "en-US");      
  }

  joinMatchVisible() {
    this.intervalUpdate = setInterval(() => {      
      this.loadMatchResult();
    }, 5000)
  }
  joinMatchGone() {
    clearInterval(this.intervalUpdate);
  }

  public compareFn(a:Heroe, b:Heroe): boolean {
    return a.id == b.id;
  }

  selectPlayer(player: Player) {
    this.loader.show();
    this.selectedPlayer = player;
    this.selectedPlayer.heroPhoto = this.getHero(player.heroId).photo;
    this.matchService.loadResult(this.selected.id, this.selectedPlayer.idUser).subscribe(
      data => {                 
        this.resultsForm.get("Level").setValue(data.level);
        this.resultsForm.get("Kills").setValue(data.kills);
        this.resultsForm.get("Dies").setValue(data.dies);
        this.resultsForm.get("Assist").setValue(data.assistans);
        this.resultsForm.get("Creeps").setValue(data.creeps);
        this.resultsForm.get("CreepsDeny").setValue(data.creepsDeny);
        this.resultsForm.get("Damage").setValue(data.damageMaked);
        this.resultsForm.get("DamageTaken").setValue(data.damageTaked);
        this.resultsForm.get("DamageBlocked").setValue(data.damageBlocked);
        this.resultsForm.get("Heal").setValue(data.hpHealed);
        this.resultsForm.get("Mana").setValue(data.manaRestore);
        this.resultsForm.get("NetValue").setValue(data.netValue);
        this.resultsForm.get("Wards").setValue(data.wards);
        this.resultsForm.get("GracePoint").setValue(data.gracePoints);
        jQuery("#player_result").modal("show");
        this.loader.hidde();
      },
      error => { this.PosibleMatchErrors(error); });

  }

  getHero(id) {
    let hero: Heroe = this.heroes.find(h => h.id == id);
    return hero == null || hero == undefined ? new Heroe({ photo: "../../../assets/images/pods_bg.webm" }) : hero;
  }

  loadinHeroComplete() {
    let heroId: number = this.selectedHero.id;
    let hero: Heroe = this.heroes.find(h => h.id == heroId);
    if (hero.photo == "../../../assets/images/pods_bg.webm") {
      this.heroService.getById(heroId.toString()).subscribe(data => {
        hero.photo = data.photo;
      });
    }
  }

  loadJoinDialog() {
    jQuery("#join_match").modal("hide");
    jQuery("#join_dialog").modal("show");
  }

  loadMatchStatus() {
    this.matchService.getById(this.selected.id.toString()).subscribe(data => {
      this.selected = data;

    });
  }

  getHeroOrLoad(idHero: number) {
    let heroId: number = idHero;
    let hero: Heroe = this.heroes.find(h => h.id == heroId);
    if (hero.photo == "../../../assets/images/pods_bg.webm") {
      this.heroService.getById(heroId.toString()).subscribe(data => {
        hero.photo = data.photo;
      });
      return hero;
    }
  }

  savePlayerResult() {
    this.resultsFormSubmitted = true;
    if (this.resultsForm.invalid) {
      return false;
    }
    this.loader.show();

    let pr: PlayerResult = new PlayerResult({
      level: +this.resultsForm.get("Level").value,
      kills: +this.resultsForm.get("Kills").value,
      dies: +this.resultsForm.get("Dies").value,
      assistans: +this.resultsForm.get("Assist").value,
      creeps: +this.resultsForm.get("Creeps").value,
      creepsDeny: +this.resultsForm.get("CreepsDeny").value,
      damageMaked: +this.resultsForm.get("Damage").value,
      damageTaked: +this.resultsForm.get("DamageTaken").value,
      damageBlocked: +this.resultsForm.get("DamageBlocked").value,
      hpHealed: +this.resultsForm.get("Heal").value,
      manaRestore: +this.resultsForm.get("Mana").value,
      netValue: +this.resultsForm.get("NetValue").value,
      wards: +this.resultsForm.get("Wards").value,
      gracePoints: +this.resultsForm.get("GracePoint").value,
    });

    this.matchService.saveResult(pr, this.selected.id, this.selectedPlayer.idUser).subscribe(data => {
      api.Message(MessageType.Success, "Resultados Guardados", "Los resultados de la partida se han guardado correctamente.");
      this.loader.hidde();
      jQuery("#player_result").modal("hide");
      this.loadMatchResult();
    }, error => this.PosibleMatchErrors(error));
    this.joinSubmitted = false;
    return false;
  }



  loadMatchResult() {
    this.matchService.matchStatus(this.selected.id.toString()).subscribe(data => {
      this.killsDire = 0;
      this.killsRadiant = 0;
      this.pointsDire = 0;
      this.pointsRadiant = 0;
 

      if (data.player1 != null) {
        this.player1 = data.player1;        
        this.getHeroOrLoad(this.player1.heroId);
        this.killsRadiant += this.player1.kills;
        this.player1.score = data.scoreRadiant1;
        this.pointsRadiant +=data.scoreRadiant1;
      
      }
      else {
        this.player1 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
      if (data.player2 != null) {
        this.player2 = data.player2;
        this.getHeroOrLoad(this.player2.heroId);
        this.killsRadiant += this.player2.kills;
        this.player2.score = data.scoreRadiant2;
        this.pointsRadiant +=data.scoreRadiant2;
      }      
      else {
        this.player2 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }      
      if (data.player3 != null) {
        this.player3 = data.player3;
        this.getHeroOrLoad(this.player3.heroId);
        this.killsRadiant += this.player3.kills;
        this.player3.score = data.scoreRadiant3;
        this.pointsRadiant +=data.scoreRadiant3;
      }
      else {
        this.player3 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }     
      if (data.player4 != null) {
        this.player4 = data.player4;
        this.getHeroOrLoad(this.player4.heroId);
        this.killsRadiant += this.player4.kills;
        this.player4.score = data.scoreRadiant4;
        this.pointsRadiant +=data.scoreRadiant4;
      }
      else {
        this.player4 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
      if (data.player5 != null) {
        this.player5 = data.player5;
        this.getHeroOrLoad(this.player5.heroId);
        this.killsRadiant += this.player5.kills;
        this.player5.score = data.scoreRadiant5;
        this.pointsRadiant +=data.scoreRadiant5;
      }
      else {
        this.player5 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
      if (data.player6 != null) {
        this.player6 = data.player6;
        this.getHeroOrLoad(this.player6.heroId);
        this.killsDire += this.player6.kills;
        this.player6.score = data.scoreDire1;
        this.pointsDire +=data.scoreDire1;
      }
      else {
        this.player6 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
      if (data.player7 != null) {
        this.player7 = data.player7;
        this.getHeroOrLoad(this.player7.heroId);
        this.killsDire += this.player7.kills;
        this.player7.score = data.scoreDire2;
        this.pointsDire +=data.scoreDire2;
      }
      else {
        this.player7 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
      if (data.player8 != null) {
        this.player8 = data.player8;
        this.getHeroOrLoad(this.player8.heroId);
        this.killsDire += this.player8.kills;
        this.player8.score = data.scoreDire3;
        this.pointsDire +=data.scoreDire3;
      }
      else {
        this.player8 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
      if (data.player9 != null) {
        this.player9 = data.player9;
        this.getHeroOrLoad(this.player9.heroId);
        this.killsDire += this.player9.kills;
        this.player9.score = data.scoreDire4;
        this.pointsDire +=data.scoreDire4;
      }
      else {
        this.player9 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
      if (data.player10 != null) {
        this.player10 = data.player10;
        this.getHeroOrLoad(this.player10.heroId);
        this.killsDire += this.player10.kills;
        this.player10.score = data.scoreDire5;
        this.pointsDire +=data.scoreDire5;
      }
      else {
        this.player10 = new Player({ hero: "-", rol: -1,score:0, heroPhoto: "../../../assets/images/pods_bg.webm", name: "Disponible", idUser: -1 });
      }
    });
  }

  leavePlay() {
    let j = new Join({ idMatch: this.selected.id });
    this.matchService.leave(j).subscribe(data => {
      api.Message(MessageType.Success, "Se ha Abandonado la Partida", "Se ha ido de la partida correctamente.");
      this.loader.hidde();
      jQuery("#join_dialog").modal("hide");
      this.loadMatchResult();
    }, error => this.PosibleLeaveErrors(error));
  }

  startMatch(item: Match) {
    this.selected = item;
    this.loader.show();
    this.matchService.initMatch(this.selected.id.toString()).subscribe(data => {
      this.loadMatchs();
      this.loader.hidde();
      api.Message(MessageType.Success, "La Partida ha Comenzado", "!La partida ya ha comenzado!");
    }, error => this.PosibleMatchErrors(error));
  }

  join() {
    this.joinSubmitted = true;
    if (this.joinForm.invalid) {
      return false;
    }
    this.loader.show();
    let rol: number = +this.joinForm.get("joinRol").value;
    let team: number = +this.joinForm.get("joinTeam").value;
    let j = new Join({ idMatch: this.selected.id, idHeroe: this.selectedHero.id, idRol: rol, team: team });
    this.matchService.join(j).subscribe(data => {
      api.Message(MessageType.Success, "Unido a la Partida", "Se ha unido a la partida correctamente.");
      this.loader.hidde();
      jQuery("#join_dialog").modal("hide");
      jQuery("#join_match").modal("show");
      this.loadMatchResult();
    }, error => this.PosibleMatchErrors(error));
    this.joinSubmitted = false;
    return false;
  }

  loadHeroes() {
    this.loadingHeroes = true;
    this.heroService.getLite().subscribe(data => {
      this.heroes = data.body;
      this.heroes.forEach(element => {
        element.photo = "../../../assets/images/pods_bg.webm";
      });
      this.loadingHeroes = false;
    }, error => {
      this.loadingHeroes = false;
      api.Message(MessageType.Error, "Héroes no Cargados", "Los héroes no se han podido cargar, refresque la página.");
    });
  }

  loadMatchs() {
    this.loader.show();
    this.matchService.get(this.page, this.maxPerPage, this.filterStatus,this.filterFrom,this.filterTo).subscribe(data => {
      this.total = +data.headers.get("X-Total-Count");
      this.items = data.body;
      this.items.forEach(element => {
        element.createdAtFormatted = formatDate(new Date(element.createdAt), "yyyy-MM-dd hh:mm a", "en-US");
        let Radiant: String[] = [];
        let Dire: String[] = [];
        if (element.uradiant1 != "") {
          Radiant.push(element.uradiant1);
        }
        if (element.uradiant2 != "") {
          Radiant.push(element.uradiant2);
        }
        if (element.uradiant3 != "") {
          Radiant.push(element.uradiant3);
        }
        if (element.uradiant4 != "") {
          Radiant.push(element.uradiant4);
        }
        if (element.uradiant5 != "") {
          Radiant.push(element.uradiant5);
        }
        element.teamRadiant = "(" + Radiant.toString() + ")";
        if (element.udire1 != "") {
          Dire.push(element.udire1);
        }
        if (element.udire2 != "") {
          Dire.push(element.udire2);
        }
        if (element.udire3 != "") {
          Dire.push(element.udire3);
        }
        if (element.udire4 != "") {
          Dire.push(element.udire4);
        }
        if (element.udire5 != "") {
          Dire.push(element.udire5);
        }
        element.teamDire = "(" + Dire.toString() + ")";

      });
      this.loader.hidde();
    }, error => { this.PosibleMatchErrors(error); });
  }

  paste() {
    var pasteEvent = new ClipboardEvent('paste');
    document.dispatchEvent(pasteEvent);
  }

  pasteClipboard(event: any) {
    const items = event.clipboardData.items;
    let blob = null;

    for (const item of items) {
      if (item.type.indexOf('image') === 0) {
        blob = item.getAsFile();
      }
    }
    if (blob !== null) {
      const fileFromBlob: File = new File([blob], 'your-filename.jpg');
      const reader = new FileReader();
      reader.onload = (evt: any) => {
        this.imgCapture = evt.target.result;
      };
      reader.readAsDataURL(blob);
    }
  }

  downloadReplay(item:Match){    
    let exist = downloadList.find(x=>x.fileName === item.replayFile);
    if(exist!=null && exist.status == DirectionDownloadStatus.InProgress){
      api.Message(MessageType.Error,"Ya se está Descargando","El replay "+item.replayFile+" ya se está descargando");
      return;
    }
    else{
      downloadList.push(new DownloadItem({fileName:item.replayFile,bytesDownloaded:0,percent:0,status:DirectionDownloadStatus.InProgress})); 
    }
    this.loader.show();    
    this.matchService.downloadFile(item.id).subscribe(data => {    
      let exist = downloadList.find(x=>x.fileName === item.replayFile && x.status == DirectionDownloadStatus.InProgress);
      switch (data.type) {
        case HttpEventType.DownloadProgress:
          exist.bytesDownloaded = data.loaded;
          exist.totalBytes = data.total;
          exist.percent = Math.round((data.loaded / data.total) * 100);              
          exist.status == DirectionDownloadStatus.InProgress;            
          break;
        case HttpEventType.Response:
          exist.status = DirectionDownloadStatus.Complete;                
          const downloadedFile = new Blob([data.body], { type: data.body.type });
          console.log(downloadedFile);
          const a = document.createElement('a');
          a.setAttribute('style', 'display:none;');
          document.body.appendChild(a);
          a.download = item.replayFile;
          a.href = URL.createObjectURL(downloadedFile);
          a.target = '_blank';
          a.click();
          document.body.removeChild(a);
          break;
      }
      this.loader.hidde();
    },error=>{      
      this.loader.hidde(); 
      let exist = downloadList.find(x=>x.fileName === item.replayFile && x.status == DirectionDownloadStatus.InProgress);          
      exist.status= DirectionDownloadStatus.Error;             
      this.PosibleMatchErrors(error);
      
    }); 
  }
  

  previewImage(item: Match){
    this.imgCapture = item.photo;    
    jQuery("#view_image").modal("show");
  }

  selectMatch(item: Match) {
    this.selected = item;    
    this.loadMatchResult();
  }

  newGame() {
    this.loader.show();
    this.matchService.add(new Match({ createdAt: (new Date().toISOString()) })).subscribe(data => {
      this.loader.hidde();
      jQuery("#create_game").modal("hide");
      api.Message(MessageType.Success, "Partida Creada", "La partida ha sido creada, esperando jugadores...");
      this.loadMatchs();
    }, error => { this.PosibleMatchErrors(error); });
  }

  cancelGame() {
    this.loader.show();
    this.matchService.cancel(this.selected.id.toString()).subscribe(data => {
      this.loader.hidde();
      jQuery("#null_match").modal("hide");
      api.Message(MessageType.Success, "Partida Anulada", "La partida ha sido anulada, no se aplicarán las puntuaciones.");
      this.loadMatchs();
    }, error => { this.PosibleMatchErrors(error); });
  }

  getCombatLogsFromPlayer(){
    this.loader.show();    
    this.matchService.getCombatLogs(this.selected.id,this.selectedPlayer.idUser).subscribe(data=>{
      this.combatLogs = data;
      this.loader.hidde();
      jQuery("#score_logs").modal("show");
    },error=>{
      this.PosibleMatchErrors(error);
    });
  }

  selectPlayerScoreLogs(player: Player) {
    this.loader.show();
    this.selectedPlayer = player;
    this.selectedPlayer.heroPhoto = this.getHero(player.heroId).photo;
    this.matchService.loadResult(this.selected.id, this.selectedPlayer.idUser).subscribe(
      data => {                
        this.selectedPlayerResult = data;
        this.getCombatLogsFromPlayer();
      },
      error => { this.PosibleMatchErrors(error); });

  }

  PosibleMatchErrors(error: HttpErrorResponse) {
    this.loader.hidde();
    console.log(error);
    switch (error.status) {
      case 0:
        api.Message(MessageType.Error, "Error de Conexión", "Compruebe la Conexión");
        break;
      case 401:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Ha expirado su sessión", "Necesita acceder nuevamente.");
        break;
      case 403:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Falta de Permisos", "Usted no tiene permisos para realizar esa acción.");
        break;
      case 404:
        api.Message(MessageType.Error, "La Partida no Existe", "No se ha encontrado la partida en el sistema.");
        this.loadMatchs();
        break;
      case 501:
        api.Message(MessageType.Error, "La Partida ya ha sido Cancelada", "La partida ya estaba cancelada");
        jQuery("#null_match").modal("hide");
        this.loadMatchs();
        break;
      case 502:
        api.Message(MessageType.Error, "Error Uniéndose a la Partida", "La partida  no está en el estado correcto (Creada).");
        jQuery("#join_match").modal("hide");
        this.loadMatchs();
        break;
      case 503:
        api.Message(MessageType.Error, "Error Uniéndose a la Partida", "Su perfil no está listo para unirse a la partida.");
        jQuery("#join_match").modal("hide");
        this.authentication.logOut();
        break;
      case 504:
        api.Message(MessageType.Error, "Error Uniéndose a la Partida", "El equipo seleccionado está lleno.");
        break;
      case 505:
        api.Message(MessageType.Error, "Error Uniéndose a la Partida", "El Héroe seleccioando no se encuentra en el sistema.");
        break;
      case 506:
        api.Message(MessageType.Error, "La Partida no puede Iniciarse", "Para comenzar la partida debe haber al menos 3 jugadores en cada equipo.");
        break;
      case 507:
        api.Message(MessageType.Error, "La Partida no está Lista", "No se encontró el Recurso MatchResult de la Partida.");
        break;
      case 508:
        api.Message(MessageType.Error, "Ranura Vacia", "No hay ningún jugador en es ranura.");
        break;
      case 509:
          api.Message(MessageType.Error, "Faltan Recursos de la Partida", "No se ha encontrado un recurso de la partida, contacte al administrador.");
          break;
      case 510:
          api.Message(MessageType.Error, "Usuario no Encontrado", "No se ha encontrado un usuario de la partida, revise los Logs para identificar el problema.");
          break;
      case 511:        
          api.Message(MessageType.Error, "Replay no Encontrado", "No se ha encontrado el replay de la partida.");
          break;
      default:
        api.Message(MessageType.Error, String.Format("Error Desconocido (Código:{0})", error.status), error.message);
        break;
    }
  }

  PosibleLeaveErrors(error: HttpErrorResponse) {
    this.loader.hidde();
    switch (error.status) {
      case 0:
        api.Message(MessageType.Error, "Error de Conexión", "Compruebe la Conexión");
        break;
      case 401:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Ha expirado su sessión", "Necesita acceder nuevamente.");
        break;
      case 403:
        this.authentication.logOut();
        api.Message(MessageType.Error, "Falta de Permisos", "Usted no tiene permisos para realizar esa acción.");
        break;
      case 404:
        api.Message(MessageType.Error, "La Partida no Existe", "No se ha encontrado la partida en el sistema.");
        this.loadMatchs();
        break;
      case 501:
        api.Message(MessageType.Error, "La Partida ya ha sido Cancelada", "La partida ya estaba cancelada");
        jQuery("#null_match").modal("hide");
        this.loadMatchs();
        break;
      case 502:
        api.Message(MessageType.Error, "Error Saliendo de la Partida", "La partida  no está en el estado correcto (Creada).");
        jQuery("#join_match").modal("hide");
        this.loadMatchs();
        break;
      case 503:
        api.Message(MessageType.Error, "Error Saliendo de la Partida", "Su perfil no está listo para unirse a la partida.");
        jQuery("#join_match").modal("hide");
        this.authentication.logOut();
        break;
      case 504:
        api.Message(MessageType.Error, "No estas en esta Partida", "El jugador no se encuentra en la partida.");
        jQuery("#join_match").modal("hide");
        this.loadMatchs();
        break;
      default:
        api.Message(MessageType.Error, String.Format("Error Desconocido (Código:{0})", error.status), error.message);
        break;
    }
  }
}
